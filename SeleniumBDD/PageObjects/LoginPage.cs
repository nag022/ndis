﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class LoginPage
    {
        public IWebDriver WebDriver;
        Actions myAction;
        public LoginPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            myAction = new Actions(WebDriver);
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

        }

        //*************************Sign In***************
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'signin')]")] private IWebElement SignInButton { get; set; }
        [FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement EmailAddress { get; set; }
        [FindsBy(How = How.Id, Using = "txtPassword")] private IWebElement Password { get; set; }
        [FindsBy(How = How.Id, Using = "btnSignIn")] private IWebElement LoginButton { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(@tabindex,'0')]")] private IWebElement SignOut { get; set; }


        //***************************Add Services*********
        [FindsBy(How = How.Id, Using = "btnSubmit")] public IWebElement SubmitButton { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'home')]")] private IWebElement Home { get; set; }
        [FindsBy(How = How.Id, Using = "btnSupportCategoryContinue")] public IWebElement CategoryContinue { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddService")] public IWebElement AddServiceButton { get; set; }


        //*****************************Add Location/Edit Location**********
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Location.name')]")] public IWebElement LocationName { get; set; }
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Location.description')]")] public IWebElement LocationDescription { get; set; }
        [FindsBy(How = How.Id, Using = "txtPhoneNo")] public IWebElement PhoneNumber { get; set; }
        [FindsBy(How = How.Id, Using = "btnEditLocation")] public IWebElement EditLocationButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] public IWebElement BtnSave { get; set; }


        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'/providerhome')]")] private IWebElement ProviderSearch { get; set; }

        //*****************************Edit Organization**********
        [FindsBy(How = How.Id, Using = "editOrganisationBtn")] private IWebElement editOrganisationBtn { get; set; }
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Organization.extension:comment.valueString')]")] private IWebElement Description { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.telecom:website.value')]")] private IWebElement WebSite { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.extension:ndis-reg-status.valueCoding')]")] private IWebElement NDISregistered { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement BtSave { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddLocation")] private IWebElement AddLocation { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'ACN')]")] private IWebElement ACN { get; set; }
        [FindsBy(How = How.Id, Using = "txtProviderNumber")] private IWebElement ProviderNumber { get; set; }

        //*****************************Edit Services**********
        //[FindsBy(How = How.XPath, Using = "//textarea[contains(@class, 'form-control.input-text.display-text.ng-pristine.ng-valid.ng-touched')]")] private IWebElement editDescription { get; set; }
        [FindsBy(How = How.XPath, Using = "//span[contains(@class, 'rg-service-display')]")] private IWebElement editDescription { get; set; }



        //*****************************Add/Edit Support Worker**********
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.name.given')]")] private IWebElement PractitionerGivenName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.name.family')]")] private IWebElement PractitionerFamilyName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.practitionerRole.role.text')]")] private IWebElement PractitionerRole { get; set; }
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Practitioner.extension:bio.valueString')]")] private IWebElement PractitionerBio { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddDifferentQualification")] private IWebElement AddDifferentQualification { get; set; }
        [FindsBy(How = How.Id, Using = "txtAddQualification")] private IWebElement PractitionerQualification { get; set; }

        [FindsBy(How = How.Id, Using = "btnAddQualification")] private IWebElement AddQualification { get; set; }
        [FindsBy(How = How.Id, Using = "btnChooseServices")] private IWebElement SelectServices { get; set; }
        [FindsBy(How = How.Id, Using = "btnSaveService")] private IWebElement SaveService { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement SaveStaff { get; set; }
        [FindsBy(How = How.Id, Using = "btnEditStaff")] private IWebElement btnEditStaff { get; set; }

        [FindsBy(How = How.Id, Using = "btnRegister")] private IWebElement SaveStaffService { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddStaff")] private IWebElement Addstaff { get; set; }



        public void LaunchApplication()
        {

            WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["appUrl"]);
            //ProviderSearch.Click();
        }


        //*************************Sign In***************

        public void CallSignPage()
        {
            //Thread.Sleep(5000);
            SignInButton.Click();
        }

        public Boolean ConfirmSigninPage()
        {
            return EmailAddress.Enabled;
        }

        public void LoginToApplication()
        {
            //Thread.Sleep(5000);
            EmailAddress.SendKeys("marketplacendis+2019@gmail.com");
            Password.SendKeys("Grisham1!");
            //EmailAddress.SendKeys(ConfigurationManager.AppSettings["username"]);
            //Password.SendKeys(ConfigurationManager.AppSettings["password"]);
            LoginButton.Click();
            //SignOut.Click();

        }

        public void SingOutApp()
        {
            SignOut.Click();
        }

        public void LogoutApplication()
        {
            // WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["appUrl"]);
        }

        public Boolean ConfirmUserLogin()
        {
            return Home.Enabled;
        }

        public void selectViewFromMyProfile()
        {
            WebDriver.FindElement(By.XPath("//a[contains(@href,'myprofile')]")).Click();
            Thread.Sleep(9000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            WebDriver.FindElements(By.ClassName("tile-action-button")).ElementAt(0).Click();

        }

        public void NavigateToMyProfileNavLinks(String profileTab)
        {
            Thread.Sleep(10000);
            IList<IWebElement> collection = WebDriver.FindElements(By.ClassName("nav-link"));
            for (int i = 0; i < collection.Count; i++)
            {
                if (profileTab.Contains(collection.ElementAt(i).Text))
                {
                    collection.ElementAt(i).Click();
                    break;
                }
            }
        }


        //*****************************Edit Organization**********

        public void clickEditOrganization()
        {
            Thread.Sleep(4000);
            editOrganisationBtn.Click();
        }

        public Boolean Descritionfield()
        {
            return Description.Enabled;
        }

        public Boolean EditButton()
        {
            return editOrganisationBtn.Enabled;
        }

        public void EDitOrganizationDetails()
        {
            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            //WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(5).Click();

            //Thread.Sleep(5000);
            //Add Logo           
            //WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            //WebDriver.FindElements(By.XPath("//input[contains(@id,'uploadProfilePhoto')]")).ElementAt(0).Click();

            //System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");

            Thread.Sleep(5000);
            //add description   
            //ACN.Clear();
            //ACN.SendKeys("123456789");
            //Description.Clear();
            //Description.SendKeys("NDIS provider");
            ////WebDriver.FindElements(By.XPath("//input[contains(@name,'Organization.extension:ndis-reg-status.valueCoding')]")).ElementAt(0).Click();
            //WebDriver.FindElement(By.Id("lblReg")).Click();
            ProviderNumber.Clear();
            ProviderNumber.SendKeys("999999999");
            //Thread.Sleep(5000);
            //Add Banner
            //WebDriver.FindElement(By.ClassName("fileupload")).Click();
            //WebDriver.FindElement(By.CssSelector("#uploadBannerImage")).Click();

            //WebDriver.FindElement(By.ClassName("fileupload")).Click();
            //WebDriver.FindElement(By.XPath("//input[contains(@class,'ng-untouched.ng-pristine.ng-valid')]")).Click();
           // System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");
            BtnSave.Click();
            //WebSite.Clear();

        }


        //*****************************Add Location**********

        public void SelectAddLocationButton()
        {
            //Thread.Sleep(10000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            AddLocation.Click();
        }

        public void addLocations()
        {
            Thread.Sleep(10000);
            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(0).Click();
            LocationName.SendKeys("123 Main");
            LocationDescription.SendKeys("123 Main Street");
            PhoneNumber.SendKeys("6123457809");
            EmailAddress.SendKeys("ndis@gmail.com");
            selectRegion("Northern Sydney");
            selectRegion("Katherine region");
            selectRegion("Brisbane");
            selectRegion("Southern Adelaide");
            selectRegion("Western Melbourne");
            selectFecility("Wheelchair access");
            selectFecility("Baby Change Facility");
            selectFecility("TTY facilities");
            BtnSave.Click();
        }




        public void selectRegion(String regionName)
        {
            ReadOnlyCollection<IWebElement> allRegions = WebDriver.FindElements(By.ClassName("checkbox"));
            for (int i = 0; i < allRegions.Count; i++)
            {
                if (allRegions.ElementAt(i).GetAttribute("innerText").Contains(regionName))
                {
                    allRegions.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void selectFecility(String fecilityName)
        {
            ReadOnlyCollection<IWebElement> allRegions = WebDriver.FindElements(By.ClassName("checkbox"));
            for (int i = 0; i < allRegions.Count; i++)
            {
                if (allRegions.ElementAt(i).GetAttribute("innerText").Contains(fecilityName))
                {
                    allRegions.ElementAt(i).Click();
                    break;
                }
            }
        }


        //*****************************Edit Location**********

        public Boolean EditLocationBtn()
        {
            return EditLocationButton.Enabled;
        }

        public void clickEditLocations()
        {
            EditLocationButton.Click();
        }

        public void EditLocations_edit()
        {
            Thread.Sleep(10000);
            WebDriver.FindElement(By.Id("allOfAustralia")).Click();
            WebDriver.FindElement(By.Id("allOfAustralia")).Click();
            selectLocation("Central Coast");
            selectLocation("Barkly");
            selectLocation("Bundaberg");
            selectLocation("Limestone Coast");
            EditFacilities();
            BtnSave.Click();
            //LocationName.Clear();
            //LocationName.SendKeys("Bourke" + GeneralUtility.randumNumber());
            //LocationDescription.Clear();
            //LocationDescription.SendKeys("NDIS");
            //PhoneNumber.Clear();
            //PhoneNumber.SendKeys("6123457809");
            //EmailAddress.Clear();
            //EmailAddress.SendKeys("ndis@gmail.com");
            //selectRegion("Northern Sydney");
            //selectRegion("Katherine region");
            //selectRegion("Brisbane");
            //selectRegion("Southern Adelaide");
            //selectRegion("Western Melbourne");
            //selectFecility("Wheelchair access");
            //selectFecility("Baby Change Facility");
            //selectFecility("TTY facilities");
            //BtnSave.Click();
        }

        public void EditFacilities()
        {
            editFacility("Accessible Telephones");
            editFacility("Disabled Parking");
            editFacility("Toilet");
        }

        public void selectLocation(String id)
        {
            WebDriver.FindElement(By.Id(id)).Click();
        }

        public void editFacility(String id)
        {
            WebDriver.FindElement(By.Id(id)).Click();
        }



        //***************************Add Services*********

        public void Addservices()
        {

            AddServiceButton.Click();

        }

        public Boolean AddServicesBtn()
        {
            //WebDriverWait wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(10));
            //wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Passwd")));

            //return WebDriver.Manage(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.Id("btnAddService")));
            Thread.Sleep(5000);
            return AddServiceButton.Enabled;
        }

        public Boolean SupportPage()
        {
            return CategoryContinue.Enabled;
        }

        public void SelectCategoriesGroups_edit()
        {
            //selectSupportCategory("Assistance With Daily Living");
            selectSupportCategory("Improved Life Choices");
            selectSupportCategory("Improved Daily Living Skills");
            CategoryContinue.Click();


           

            //Thread.Sleep(2000);
            //Home.SendKeys(Keys.PageUp);

            //chooseService("Accommodation/Tenancy");
            //Thread.Sleep(2000);
            //WebDriver.FindElements(By.Id("chkNDISRegistered0010101")).ElementAt(0).Click();
            ////WebDriver.FindElement(By.Id("chkNDISRegistered0010101")).Click();

            ////WebDriver.FindElements(By.Id("dcf988d751e3439f83509b529597305f0010101")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Name("0010101")).ElementAt(1).Click();
            //SubmitButton.Click();

            Thread.Sleep(2000);
            Home.SendKeys(Keys.PageUp);

            chooseService("Plan Management");
            Thread.Sleep(2000);
            //WebDriver.FindElements(By.Id("0127")).ElementAt(0).Click();
            WebDriver.FindElements(By.Id("chkNDISRegistered0140127")).ElementAt(1).Click();
            //WebDriver.FindElements(By.Id("dd1d67c5b4b1417da7e31268351a090f0140127")).ElementAt(1).Click();
            //WebDriver.FindElements(By.Name("0140127")).ElementAt(1).Click();

            SubmitButton.Click();
            Thread.Sleep(2000);
            Home.SendKeys(Keys.PageUp);

            chooseService("Custom Prosthetics");
            Thread.Sleep(2000);
            //WebDriver.FindElements(By.Id("0135")).ElementAt(0).Click();
            WebDriver.FindElements(By.Id("chkNDISRegistered0150135")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Id("dd1d67c5b4b1417da7e31268351a090f0150135")).ElementAt(1).Click();


            chooseService("Development of Daily Living and Life Skills");
            Thread.Sleep(2000);
            //WebDriver.FindElements(By.Id("0117")).ElementAt(0).Click();
            WebDriver.FindElements(By.Id("chkNDISRegistered0150117")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Id("dd1d67c5b4b1417da7e31268351a090f0150117")).ElementAt(1).Click();


            chooseService("Therapeutic Supports");
            Thread.Sleep(2000);
            //WebDriver.FindElements(By.Id("0128")).ElementAt(0).Click();
            WebDriver.FindElements(By.Id("chkNDISRegistered0150128")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Id("dd1d67c5b4b1417da7e31268351a090f0150128")).ElementAt(1).Click();
            SubmitButton.Click();

            //DoThisLaterButton.Click();
        }


        public void selectSupportCategory(String supportCategory)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(supportCategory))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }
        public void selectService(String serviceName)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void chooseService(String serviceName)
        {
            //ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            //ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("pt15px"));
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("pt0px"));

            for (int i = 0; i < allAssistance.Count; i++)
            {
                if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allAssistance.ElementAt(i).Click();
                    break;
                }
            }
        }


        //*****************************Edit Services**********

        public void EditService()
        {
            Thread.Sleep(5000);
            //WebDriver.FindElements(By.ClassName("provider-button-dark-small")).ElementAt(0).Click();
            WebDriver.FindElements(By.XPath("//button[contains(@class, 'provider-button-dark-small')]")).ElementAt(2).Click();
        }

        public bool EditServicePage()
        {
            return WebDriver.FindElement(By.ClassName("pad")).GetAttribute("innerText").Trim().Equals("Which locations provide this service? ");
        }

        public void EditServices()
        {
            //editDescription.Clear();
            //editDescription.SendKeys("Service");
            editDescription.Click();
            SaveStaffService.Click();

            AddServiceButton.SendKeys(Keys.PageUp);
        }


        //public void addServices_edit()
        //{
        //    //NavigateToMyProfileNavLinks("Services");
        //    //AddServiceButton.Click();
        //    selectSupportCategory("Assistance With Daily Living");
        //    selectSupportCategory("Transport");
        //    selectSupportCategory("Consumables");
        //    CategoryContinue.Click();
        //    Thread.Sleep(5000);
        //    //Assistance With Daily Living
        //    ChooseServices("Accommodation/Tenancy", 0);
        //    ChooseServices("Daily Personal Activities", 6);
        //    SubmitButton.Click();
        //    //Transport
        //    ChooseServices("Assistance with Travel and/or Transport", 0);
        //    SubmitButton.Click();
        //    //Consumables
        //    ChooseServices("Assistive products for personal care and safety", 0);
        //    SubmitButton.Click();

        //}

        public void ChooseServices(String supportCategory, int number)
        {
            chooseService(supportCategory);
            ndisRegisteredStatus(number);
        }

        public void ndisRegisteredStatus(int number)
        {
            //ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            Thread.Sleep(5000);
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            Thread.Sleep(5000);
            allAssistance.ElementAt(number).Click();
            allAssistance.ElementAt(number + 2).Click();
            allAssistance.ElementAt(number + 3).Click();
            allAssistance.ElementAt(number + 4).Click();
            allAssistance.ElementAt(number + 5).Click();
        }



        //*****************************Add Support Worker**********

        public void SelectAddStaff()
        {
            Addstaff.Click();
        }

        public void AddSupportWorkerDetails()
        {
            PractitionerGivenName.SendKeys("John");
            PractitionerFamilyName.SendKeys("Grisham");
            PractitionerRole.SendKeys("Support Worker");



            //SelectElement mydropDown = new SelectElement(WebDriver.FindElement(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")));
            //mydropDown.SelectByIndex(1);

            //SelectElement mydropDown = new SelectElement(WebDriver.FindElement(By.ClassName("fhirq-select")));
            //mydropDown.SelectByValue("Male");


            //WebDriver.FindElements(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")).ElementAt(1).Click();


            PractitionerBio.SendKeys("Experienced");

            Thread.Sleep(2000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            //WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            //System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");

            Thread.Sleep(5000);

            //WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();

            WebDriver.FindElement(By.Id("chkQualifications_1")).Click();

            AddDifferentQualification.Click();
            PractitionerQualification.SendKeys("Safety Hazard Level 4");
            AddQualification.Click();

            Thread.Sleep(2000);

            WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();

            Thread.Sleep(2000);

            WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(3).Click();

            //WebDriver.FindElements(By.Id("allLocations")).ElementAt(0).Click();

            SelectServices.Click();

            //AssitanceWithDailyLiving("Accommodation/Tenancy");

            Thread.Sleep(4000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            //WebDriver.FindElements(By.Id("chk0101")).ElementAt(0).Click();
            if (WebDriver.FindElements(By.Name("chkService")).Count > 0)
                WebDriver.FindElements(By.Name("chkService")).ElementAt(0).Click();
                WebDriver.FindElements(By.Name("chkService")).ElementAt(2).Click();


            Thread.Sleep(2000);
            SaveService.Click();

            SaveStaff.Click();

        }


        //*****************************Add/Edit Support Worker**********

        public void EditStaffButton()
        {

            Thread.Sleep(5000);
            btnEditStaff.Click();
        }

        public Boolean EditStaffPage()
        {
            return PractitionerGivenName.Enabled;
        }

        public void EditSupportWorkerDetails()
        {
            //PractitionerGivenName.SendKeys("Wild");
            //PractitionerFamilyName.SendKeys("Animal");
            //PractitionerRole.SendKeys("Support Worker");


            //Thread.Sleep(4000);
            //SelectElement mydropDown = new SelectElement(WebDriver.FindElement(By.XPath("//select[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")));
            //mydropDown.SelectByIndex(1);

            //SelectElement mydropDown = new SelectElement(WebDriver.FindElements(By.ClassName("fhirq-select")).Click());
            //mydropDown.SelectByValue("Male");


            //WebDriver.FindElements(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")).ElementAt(1).Click();

            PractitionerBio.Clear();
            PractitionerBio.SendKeys("Experienced");

            Thread.Sleep(2000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            //WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            //System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");

            Thread.Sleep(5000);

            //WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();

            WebDriver.FindElements(By.Id("chkQualifications")).ElementAt(2).Click();

            AddDifferentQualification.Click();
            PractitionerQualification.SendKeys("Safety Hazard Level 4");
            AddQualification.Click();

            //WebDriver.FindElements(By.Id("75c1a5244e8843c9a2f1f003b49f749f")).ElementAt(0).Click();
            //WebDriver.FindElement(By.Id("75c1a5244e8843c9a2f1f003b49f749f")).Click();
            //WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(4).Click();

            SelectServices.Click();
            //AssitanceWithDailyLiving("Accommodation/Tenancy");

            Thread.Sleep(4000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            //WebDriver.FindElements(By.Id("chk0101")).ElementAt(0).Click();
            if (WebDriver.FindElements(By.Name("chkService")).Count > 0)

                WebDriver.FindElements(By.Name("chkService")).ElementAt(0).Click();
            WebDriver.FindElements(By.Name("chkService")).ElementAt(4).Click();

            Thread.Sleep(2000);
            SaveService.Click();


            SaveStaff.Click();

        }

        public Boolean StaffTabPage()
        {
            return Addstaff.Enabled;
        }
    }


}

