﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Threading;

namespace SeleniumBDD.StepDefinitions
{
    public class ReviewsandRatingsPage
    {
        private IWebDriver WebDriver;

        public ReviewsandRatingsPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
        }

        [FindsBy(How = How.XPath, Using = "//a(contains[@href, '/allratingandreview'])")] private IWebElement RatinsgandReviewOption { get; set; }
        [FindsBy(How = How.Id, Using = "btnCancel")] private IWebElement CancelBtn { get; set;  }
        //[FindsBy(How = How.Id, Using = "")] private IWebElement  {}


        public bool MyChoicesOption()
        {
            return WebDriver.FindElement(By.ClassName("dropdown-toggle")).Enabled;

        }

        public void MyChoices()
        {
            WebDriver.FindElement(By.ClassName("dropdown-toggle")).Click();
        }

        public bool RandROption()
        {
            return RatinsgandReviewOption.Enabled;
        }

        public void ReviewsRatings()
        {
            RatinsgandReviewOption.Click();
        }

        public bool Cancel()
        {
            return CancelBtn.Enabled;
        }

        public void MyPlanPage()
        {
            Thread.Sleep(2000);
            CancelBtn.Click();

        }
    }
}