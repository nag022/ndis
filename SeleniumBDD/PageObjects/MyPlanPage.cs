﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace SeleniumBDD.StepDefinitions
{
    public class MyPlanPage
    {
        private IWebDriver WebDriver;

        public MyPlanPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(45);

        }

        [FindsBy(How = How.ClassName, Using = "consumer-button-dark-large")] private IWebElement SavePlan { get; set; }
        [FindsBy(How = How.ClassName, Using = "x")] private IWebElement Message { get; set; }
        [FindsBy(How = How.ClassName, Using = "consumer-button-light-large")] private IWebElement EditPlan { get; set; }
        [FindsBy(How = How.Id, Using = "btnSupportCategoryContinue")] public IWebElement CategoryContinue { get; set; }
        [FindsBy(How = How.Id, Using = "suburbPostcode")] private IWebElement Suburb { get; set; }
        [FindsBy(How = How.ClassName, Using = "consumer-button-dark-large")] private IWebElement Continue { get; set; }




        public void SaveMyPlan()
        {
            SavePlan.Click();
        }

        public bool SuccessfulMessage()
        {
            return Message.Enabled;
        }

        public void EditMyPlan()
        {
            EditPlan.Click();
        }

        public void SelectSupportCategoy()
        {
            SelectService("Assistance With Daily Living");

            
            Boolean visible = false; // assume it is invisible
            try
            {

                IWebElement Subject = WebDriver.FindElements(By.ClassName("count-yes-no-button")).ElementAt(1);
                // if I get to here the element exists
                // if it is visible
                if (Subject.Displayed)
                    visible = true;

                Subject.Click();
            }
            catch (Exception e)
            {
                Assert.IsFalse(visible);
            }


            SelectService("Consumables");

            Thread.Sleep(5000);
            Suburb.Clear();
            Suburb.SendKeys("RINGWOOD NORTH, 3134 VIC");
            Suburb.SendKeys(Keys.ArrowDown);
            Thread.Sleep(900);
            Suburb.SendKeys(Keys.Tab);

            CategoryContinue.Click();

            if (WebDriver.FindElement(By.Id("option")).Enabled)
            {
                WebDriver.FindElements(By.Id("option")).ElementAt(1).Click();
                Continue.Click();
            }


        }

        public void SelectService(String serviceName)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }


        public bool Warning()
        {
            return WebDriver.FindElement(By.ClassName("count-yes-no-button")).Enabled;

        }

        public void SelectYES()
        {
            WebDriver.FindElements(By.ClassName("count-yes-no-button")).ElementAt(0);
        }
    }
}