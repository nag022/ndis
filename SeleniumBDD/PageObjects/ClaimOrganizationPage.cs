﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using SeleniumBDD.Utilities;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Text;


namespace SeleniumBDD.StepDefinitions
{
    public class ClaimOrganizationPage
    {
        public IWebDriver WebDriver;
        Actions myAction;
        public String OrgName = "Test Organisation XXA 20180214";
        public ClaimOrganizationPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            myAction = new Actions(WebDriver);
            PageFactory.InitElements(WebDriver, this);
            this.WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
        }


        [FindsBy(How = How.Id, Using = "txtBusinessName")] private IWebElement GiveProviderName { get; set; }
        [FindsBy(How = How.Id, Using = "btnSearch")] private IWebElement SubmitSearch { get; set; }

        [FindsBy(How = How.Id, Using = "btnClaim")] private IWebElement Claimbutton { get; set; }
        [FindsBy(How = How.Id, Using = "btnSetupProfile")] private IWebElement SetUpProfile { get; set; }

        //[FindsBy(How = How.Id, Using = "txtName")] private IWebElement Name { get; set; }
        //[FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement Email { get; set; }
        //[FindsBy(How = How.Id, Using = "txtSuburb")] private IWebElement SuburbPostcode { get; set; }
        //[FindsBy(How = How.Id, Using = "txtContactNo")] private IWebElement PhoneNumber { get; set; }
        [FindsBy(How = How.Id, Using = "txtJobTitle")] private IWebElement JobTitle { get; set; }
        [FindsBy(How = How.Id, Using = "chkBehalfOrganisation")] public IWebElement IdeclareChkBox { get; set; }
        [FindsBy(How = How.Id, Using = "chkSecurityCodeNotReceived")] public IWebElement SecurityCodeNotReceived { get; set; }
        [FindsBy(How = How.Id, Using = "txtSecurityCode")] public IWebElement EnterSecurityCode { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement BtnSave { get; set; }




        public void SearchOrganizationByName()
        {
            //ProviderSearch.Click();
            //GiveProviderName.SendKeys("ClaimOrgName");
            GiveProviderName.SendKeys(OrgName);
            SubmitSearch.Click();
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);

        }

        public Boolean ClaimOption()
        {
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);
            return Claimbutton.Enabled;

        }
        public void ClaimButton()
        {
            //WebDriver.FindElements(By.Id("btnClaim")).ElementAt(1).Click();
            Claimbutton.Click();
        }


        public Boolean ClaimThisOrganization()
        {
            return JobTitle.Enabled;
        }

        public void SubmitClaimDetails()
        {

            BtnSave.Click();
        }

        public async Task<string> GetSecurityCodeAsync()
        {

            HttpClient client = new HttpClient();
            //HttpResponseMessage response = await client.GetAsync("http://ndis-api-dev.npd.telstrahealth.com/api/v1/userorg/get/securitycode/" + OrgName);
            HttpResponseMessage response = await client.GetAsync("http://ndis-api-tst.npd.telstrahealth.com/api/v1/userorg/get/securitycode/" + OrgName);
            //HttpResponseMessage response = await client.GetAsync("https://waauenprdapindis.nonprod.mychooser.com.au/api/v1/userorg/get/securitycode/" + OrgName);

            if (response.IsSuccessStatusCode)
            {
                var jsonData = await response.Content.ReadAsStringAsync();
                dynamic data = JObject.Parse(jsonData);
                securityCode = data.DataObject[0].SecurityCode.Value;
            }
            return securityCode.ToString();
        }

        String securityCode;
        public void ClaimTheOrganization()
        {
            //Claimbutton.Click();
            JobTitle.Clear();
            JobTitle.SendKeys("CEO");
            Thread.Sleep(2000);
            IdeclareChkBox.Click();
            Thread.Sleep(2000);
            SecurityCodeNotReceived.Click();
            BtnSave.Click();
        }
        public void ClaimTheOrganizationWithSecurityCode()
        {
            //Claimbutton.Click();
            JobTitle.Clear();
            JobTitle.SendKeys("CEO");
            Thread.Sleep(4000);
            IdeclareChkBox.Click();
            Thread.Sleep(2000);
            EnterSecurityCode.SendKeys(securityCode);
            BtnSave.Click();
        }

        public bool ConfirmClaimOrganizationName()
        {
            return WebDriver.FindElement(By.ClassName("page-header1")).GetAttribute("innerText").Trim().Equals(OrgName);

        }

    }

}