﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class ProviderSearchPage
    {
        public IWebDriver WebDriver;
        Actions myAction;
        public String OrgName = "devtester2" + GeneralUtility.randumNumber();
        public ProviderSearchPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            myAction = new Actions(WebDriver);
            PageFactory.InitElements(WebDriver, this);
            //WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);
        }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'/provider')]")] private IWebElement ProviderSearch { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'providersearch')]")] private IWebElement ProvidersSearch { get; set; }
        [FindsBy(How = How.Id, Using = "txtBusinessName")] private IWebElement GiveProviderName { get; set; }
        [FindsBy(How = How.Id, Using = "btnSearch")] private IWebElement SubmitSearch { get; set; }
        [FindsBy(How = How.Id, Using = "btnRegNewOrganization")] private IWebElement RegisterNewOrganization { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.name')]")] private IWebElement TradingName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'legalEntityName')]")] private IWebElement LegalEntityName { get; set; }
        [FindsBy(How = How.Id, Using = "txtAbn")] private IWebElement ABN { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.address.line')]")] private IWebElement HeadOfficeAddress { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.address.postalCode')]")] private IWebElement PostCode { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.telecom:website.value')]")] private IWebElement WebSite { get; set; }
        [FindsBy(How = How.Id, Using = "txtPhoneNo")] private IWebElement PhoneNumber { get; set; }
        [FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement EMail { get; set; }

        [FindsBy(How = How.Id, Using = "btnContinue")] private IWebElement BtnContinue { get; set; }
        [FindsBy(How = How.Id, Using = "btnSetupProfile")] private IWebElement SetUpProfile { get; set; }
        [FindsBy(How = How.Id, Using = "txtJobTitle")] private IWebElement JobTitle { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'/home')]")] private IWebElement Home { get; set; }
        [FindsBy(How = How.Id, Using = "btnClaim")] private IWebElement Claimbutton { get; set; }

        [FindsBy(How = How.Id, Using = "chkBehalfOrganisation")] public IWebElement IdeclareChkBox { get; set; }
        [FindsBy(How = How.Id, Using = "chkSecurityCodeNotReceived")] public IWebElement SecurityCodeNotReceived { get; set; }
        [FindsBy(How = How.Id, Using = "txtSecurityCode")] public IWebElement EnterSecurityCode { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement BtnSave { get; set; }

        [FindsBy(How = How.Name, Using = "password")] public IWebElement gmailPassWord { get; set; }


        //*************************Sign In***************
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'signin')]")] private IWebElement SignInButton { get; set; }
        [FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement EmailAddress { get; set; }
        [FindsBy(How = How.Id, Using = "txtPassword")] private IWebElement Password { get; set; }
        [FindsBy(How = How.Id, Using = "btnSignIn")] private IWebElement LoginButton { get; set; }


        public void SelectProvider()
        {
            ProviderSearch.Click();
        }

        public void SelectProvidersSearch()
        {
            ProvidersSearch.Click();
        }

        public Boolean SearchOrgPage()
        {
            return GiveProviderName.Enabled;
        }


        public void SearchOrganizationByName()
        {
            //ProviderSearch.Click();
            //GiveProviderName.SendKeys(ConfigurationManager.AppSettings["SearchOrgName"]);
            Thread.Sleep(2000);
            GiveProviderName.SendKeys(OrgName);
            
            SubmitSearch.Click();

            Thread.Sleep(5000);
        }

        public Boolean OrganizationNotAvailableInDataBase()
        {
            return RegisterNewOrganization.Enabled;
        }

        public Boolean RegisterNewOrganizationOption()
        {
            return RegisterNewOrganization.Enabled;
        }
        public void SelectRegisterOrganization()
        {
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);
            Thread.Sleep(4000);
            RegisterNewOrganization.Click();
        }
        public Boolean ConfirmRegistrationPage()
        {
            return TradingName.Enabled;
        }

        public void EnterRegistrationDetails()

        {
            ABN.SendKeys(ConfigurationManager.AppSettings["Org ABN"]);

            TradingName.SendKeys(OrgName);
            LegalEntityName.SendKeys(ConfigurationManager.AppSettings["orgLegalEntity"]);
            HeadOfficeAddress.SendKeys(ConfigurationManager.AppSettings["Org Address"]);

            //Thread.Sleep(10000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);

            //PostCode.SendKeys(ConfigurationManager.AppSettings["PostCode"]);
            PostCode.SendKeys("MELBOURNE, 3004 VIC");
            PostCode.SendKeys(Keys.ArrowDown);
            PostCode.SendKeys(Keys.ArrowDown);
            Thread.Sleep(900);

            PostCode.SendKeys(Keys.Enter);
            PostCode.SendKeys(Keys.Tab);

            //WebSite.SendKeys(ConfigurationManager.AppSettings["WebSite"]);

            //PhoneNumber.SendKeys("6123457809");          
            //EMail.SendKeys("email");
            PhoneNumber.SendKeys(ConfigurationManager.AppSettings["phNumber"]);
            //EMail.SendKeys(ConfigurationManager.AppSettings["email"]);

        }


        public void SubmitRegistrationForm()
        { 

            BtnContinue.Click();

            Thread.Sleep(5000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);


            //Home.Click();
        }
        


        public Boolean ClaimOrganization()
        {
            return JobTitle.Enabled;
        }



        //public Boolean ConfirmRegistration()
        //{
        //    //String successMsg = WebDriver.FindElements(By.ClassName("main-content-text")).ElementAt(0).GetAttribute("innerText");
        //    //return (successMsg.Contains("Thank you for registering with My Life, My Choices"));

        //    return SetUpProfile.Enabled;
        //}

        


        public bool ConfirmOrganizationName()
        {
            return WebDriver.FindElement(By.ClassName("name")).GetAttribute("innerText").Trim().Equals(OrgName);
            
        }


        public void SelectProviderSearch()
        {
            Thread.Sleep(5000);

            Home.Click();

        }

        public void ClaimButton()
        {
            Claimbutton.Click();
        }

        public async Task<string> GetSecurityCodeAsync()
        {

            HttpClient client = new HttpClient();
            //HttpResponseMessage response = await client.GetAsync("http://ndis-api-dev.npd.telstrahealth.com/api/v1/userorg/get/securitycode/" + OrgName);
            //HttpResponseMessage response = await client.GetAsync("http://ndis-api-tst.npd.telstrahealth.com/api/v1/userorg/get/securitycode/" + OrgName);
            HttpResponseMessage response = await client.GetAsync("https://waauenprdapindis.nonprod.mychooser.com.au/api/v1/userorg/get/securitycode/" + OrgName);

            if (response.IsSuccessStatusCode)
            {
                var jsonData = await response.Content.ReadAsStringAsync();
                dynamic data = JObject.Parse(jsonData);
                securityCode = data.DataObject[0].SecurityCode.Value;
            }
            return securityCode.ToString();
        }
        String securityCode;
       
        public void ClaimTheOrganizationWithSecurityCode()
        {
            //Claimbutton.Click();
            //JobTitle.Clear();
            JobTitle.SendKeys("CEO");
            Thread.Sleep(4000);
            IdeclareChkBox.Click();
            Thread.Sleep(2000);
            EnterSecurityCode.SendKeys(securityCode);
            BtnSave.Click();
        }

        public Boolean SetUpMyProfileOption()
        {
            return SetUpProfile.Enabled;
        }

        public bool ConfirmClaim()
        {
            //return WebDriver.FindElement(By.ClassName("page-header1")).GetAttribute("innerText").Trim().Equals("Thank you!");
            return Home.Enabled;
        }

        public void LoginApplication()
        {
            //Thread.Sleep(5000);
            EmailAddress.SendKeys(OrgName);
            Password.SendKeys("Grisham1!");
            //EmailAddress.SendKeys(ConfigurationManager.AppSettings["username"]);
            //Password.SendKeys(ConfigurationManager.AppSettings["password"]);
            LoginButton.Click();

        }
    }
}
