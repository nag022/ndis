﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace SeleniumBDD.StepDefinitions
{
    public class AddLocationPage
    {
        public IWebDriver WebDriver;
        public String PhNo = "123456" + GeneralUtility.randumNumber();

        public AddLocationPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

        }

        [FindsBy(How = How.Id, Using = "btnAddLocation")] private IWebElement AddLocation { get; set; }

        //[FindsBy(How = How.Id, Using = "//input[contains(@data-fhirq-question-link-id,'Location.name')]")] private IWebElement LocationName { get; set; }
        //[FindsBy(How = How.Id, Using = "//textarea[contains(@data-fhirq-question-link-id, 'Location.description')]")] private IWebElement Description { get; set; }
        [FindsBy(How = How.Id, Using = "//input[contains(@data-fhirq-question-link-id, 'Location.address.line')")] private IWebElement StAddress { get; set; }
        [FindsBy(How = How.Id, Using = "//input[contains(@data-fhirq-question-link-id, 'Location.address.postalCode')]")] private IWebElement SubUrbObj { get; set; }
        //[FindsBy(How = How.Id, Using = "//input[contains(@data-fhirq-question-link-id, 'Location.telecom.value')]")] private IWebElement PhNumber { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Location.name')]")] private IWebElement LocationName { get; set; }
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Location.description')]")] private IWebElement LocationDescription { get; set; }
       

        [FindsBy(How = How.Id, Using = "txtPhoneNo")] private IWebElement PhoneNumber { get; set; }
        [FindsBy(How = How.Id, Using = "txtFaxNo")] private IWebElement FaxNo { get; set; }
        [FindsBy(How = How.Id, Using = "txtWebsite")] private IWebElement Website { get; set; }
        [FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement EMail { get; set; }



        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement BtnSave { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddAnotherLocation")] private IWebElement BtnAddAnotherLocation { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddService")] private IWebElement BtnAddService { get; set; }


        public void SelectAddLocationButton()
        {
            //Thread.Sleep(10000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            AddLocation.Click();
        }

        public Boolean AddLocationName()
        {
            return LocationName.Enabled;
        }
       

        //public void EnterLocationDetails()
        //{
        //    LocationName.SendKeys("Bourke Place");
        //    Description.SendKeys("Its a Bourke Palace");
        //    //AddressLine.SendKeys("55, Bourke St");
        //    //AddressPostalCode.SendKeys("Melbourne, VIC, 3002");
        //    PhNumber.SendKeys("7878787879");

        //    WebDriver.FindElement(By.Id("Northern Sydney")).Click();
        //    WebDriver.FindElement(By.Id("Wheelchair access")).Click();
        //    BtnSave.Click();
        //}


        public void EnterLocContactDetails()
        {
            //Thread.Sleep(10000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);

            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            //WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(0).Click();
            LocationName.SendKeys("Bourke Place");
            //LocationDescription.SendKeys("Its a Bourke Palace");
            //Thread.Sleep(10000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            PhoneNumber.SendKeys("6123457809");
            FaxNo.SendKeys("6123457809");
            //Website.SendKeys("http://telstra.com.au");
            //EMail.SendKeys("bharath.kumargaddam@telstra.helath.com");

            SelectRegion("Northern Sydney");
            SelectRegion("Katherine region");
            SelectRegion("Brisbane");
            SelectRegion("Southern Adelaide");
            SelectRegion("Western Melbourne");
            SelectFacility("Wheelchair access");
            SelectFacility("Baby Change Facility");
            SelectFacility("TTY facilities");
            
        }

        public void SelectSave()
        {
            BtnSave.Click();
        }

        public void SelectRegion(String RegionName)
        {
            ReadOnlyCollection<IWebElement> allRegions = WebDriver.FindElements(By.ClassName("checkbox"));
            for (int i = 0; i < allRegions.Count; i++)
            {
                if (allRegions.ElementAt(i).GetAttribute("innerText").Contains(RegionName))
                {
                    allRegions.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void SelectFacility(String FacilityName)
        {
            ReadOnlyCollection<IWebElement> allRegions = WebDriver.FindElements(By.ClassName("checkbox"));
            for (int i = 0; i < allRegions.Count; i++)
            {
                if (allRegions.ElementAt(i).GetAttribute("innerText").Contains(FacilityName))
                {
                    allRegions.ElementAt(i).Click();
                    break;
                }

            }
        }
        public Boolean AddAnotherLocationButton()
        {
            return BtnAddAnotherLocation.Enabled;
        }

        public Boolean AddServicesButton()
        {
            return BtnAddService.Enabled;
        }

        public void AnotherLocContactDetails()
        {
            //Thread.Sleep(10000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);

            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            //WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(0).Click();
            LocationName.SendKeys("Bourke Place" + GeneralUtility.randumNumber());
            LocationDescription.Clear();
            LocationDescription.SendKeys("Its a Paradise Palace");
            StAddress.SendKeys("60,Bourke st");

            Thread.Sleep(15000);

            SubUrbObj.SendKeys("MELBERGEN");
            Thread.Sleep(2000);

            //SubUrbObj.SendKeys(Keys.ArrowDown);
            SubUrbObj.SendKeys(Keys.Enter);

            //Thread.Sleep(10000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            PhoneNumber.SendKeys(PhNo);
            FaxNo.SendKeys("6123457809");
            //Website.SendKeys("http://telstra.com.au");
            //EMail.SendKeys("bharath.kumargaddam@telstra.helath.com");

            SelectRegion("Northern Sydney");
            SelectRegion("Katherine region");
            SelectRegion("Brisbane");
            SelectRegion("Southern Adelaide");
            SelectRegion("Western Melbourne");
            SelectFacility("Wheelchair access");
            SelectFacility("Baby Change Facility");
            SelectFacility("TTY facilities");
            BtnSave.Click();
        }

        public void selectViewFromMyProfile()
        {
            WebDriver.FindElement(By.XPath("//a[contains(@href,'myprofile')]")).Click();
            Thread.Sleep(5000);
            WebDriver.FindElements(By.ClassName("tile-action-button")).ElementAt(0).Click();

        }

        public void selectLocation(String id)
        {
            WebDriver.FindElement(By.Id(id)).Click();
        }

        public void editFacility(String id)
        {
            WebDriver.FindElement(By.Id(id)).Click();
        }   

        public void EditLocations()
        {
            Thread.Sleep(10000);
            // WebDriver.FindElement(By.Id("allOfAustralia")).Click();
            // WebDriver.FindElement(By.Id("allOfAustralia")).Click();
            selectLocation("Central Coast");
            selectLocation("Barkly");
            selectLocation("Bundaberg");
            selectLocation("Limestone Coast");
            EditFacilities();
            BtnSave.Click();
        }

        public void EditFacilities()
        {
            editFacility("Accessible Telephones");
            editFacility("Disabled Parking");
            editFacility("Toilet");
        }

        public void clickEditLocations()
        {
            WebDriver.FindElement(By.Id("btnEditLocation")).Click();
        }

    }

    
}