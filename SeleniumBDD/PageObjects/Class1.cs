﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class YahooPage
    {
        public IWebDriver webDriver;
        public Actions myAction;
        TableElement _webTable;
        int iteration;
        String emailSubject1 = "Registration Confirmation You have successfully completed your registration";
        String emailSubject2 = "Your NDIS security code";
        public YahooPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
            myAction = new Actions(webDriver);
            PageFactory.InitElements(this.webDriver, this);
            //WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            //myInbox = new TableElement(webDriver.FindElement(By.XPath("//table[contains(@class,'zt')]")));
        }

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'https://ndis-web-tst.npd.telstrahealth.com/registration/verify?invitetoken=ndis.4preedk5v90jd2tbga0h&id=fb0a7f00-7379-4e5b-bde4-2cdc9e771735')]")] private IWebElement CompleteRegistration { get; set; }
        [FindsBy(How = How.Id, Using = "login-username")] public IWebElement gmailUserName { get; set; }
        [FindsBy(How = How.Id, Using = "login-passwd")] public IWebElement gmailPassWord { get; set; }
        [FindsBy(How = How.Id, Using = "login-signin")] public IWebElement gmailNext { get; set; }


        
        [FindsBy(How = How.XPath, Using = "//div[contains(@data-name,'infinite-scroll-content')]")] public IWebElement myInbox { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'signin')]")] private IWebElement SignInButton { get; set; }

        //[FindsBy(How = How.XPath, Using = "//div[contains(@tabindex,'0')]")] private IWebElement Compose { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[contains(@data-test-id,'message-subject')]")] private IWebElement Compose { get; set; }


        public void launchGmail()
        {
            //webDriver.Manage().Cookies.DeleteAllCookies();
            webDriver.Navigate().GoToUrl("https://login.yahoo.com/?.src=ym&.intl=us&.done=https%3A%2F%2Fmail.yahoo.com%2F");



        }

        public void YahooLogin()
        {
           

                //gmailUserName.SendKeys(ConfigurationManager.AppSettings["GmailUserName"] + "@gmail.com");
                gmailUserName.SendKeys("choicezforme");
            webDriver.FindElement(By.XPath("//label[contains(@for,'persistent')]")).Click();
                gmailNext.Click();

          

            gmailPassWord.SendKeys("Grisham1!");
            gmailNext.Click();           
        }


        //public void GmailLogin()
        //{
        //    //webDriver.Manage().Cookies.DeleteAllCookies();
        //    //gmailUserName.SendKeys(ConfigurationManager.AppSettings["GmailUserName"] + "@gmail.com");

        //    //gmailNext.Click();
        //    Thread.Sleep(2000);
        //    gmailPassWord.SendKeys("Grisham1!");
        //    gmailNext.Click();
        //    Thread.Sleep(2000);
        //    webDriver.Navigate().Refresh();

        //}

        //public void ScenarioLogin()
        //{
        //    //webDriver.Manage().Cookies.DeleteAllCookies();
        //    //gmailUserName.SendKeys(ConfigurationManager.AppSettings["GmailUserName"] + "@gmail.com");
        //    gmailUserName.SendKeys("marketplacendis@gmail.com");
        //    gmailNext.Click();
        //    Thread.Sleep(2000);
        //    gmailPassWord.SendKeys("Grisham1!");
        //    gmailNext.Click();
        //    Thread.Sleep(2000);
        //    webDriver.Navigate().Refresh();

        //}

        public void DeleteRegistrationEmail()
        {

            bool flag = false;
            IList<IWebElement> allRows = myInbox.FindElements(By.TagName("li"));
            for (int i = 0; i < allRows.Count; i++)
            {

               
                for (int j = 0; j < allRows.ElementAt(i).FindElements(By.TagName("button")).Count; j++)

                {
                    Debug.WriteLine(allRows.ElementAt(i).FindElements(By.TagName("button")).ElementAt(j).Text);
                    string actualText = allRows.ElementAt(i).FindElements(By.TagName("button")).ElementAt(j).Text;
                    if (actualText.Contains(emailSubject1) || actualText.Contains(emailSubject2))
                    {
                        allRows.ElementAt(i).FindElements(By.TagName("button")).ElementAt(1).Click();
                        flag = true;
                        break;
                    }
                }
                if (flag)
                {
                    break;
                }
            }
            if (flag)
            {
                webDriver.FindElement(By.XPath("//button[contains(@data-test-id,'toolbar-delete')]")).Click();
            }

        }

        public void Subject()
        {
            if (Compose.Equals(emailSubject2));
            {
                webDriver.FindElement(By.XPath("//button[contains(@data-test-id,'icon-btn-checkbox')]")).Click();

                webDriver.FindElement(By.XPath("//button[contains(@data-test-id,'toolbar-delete')]")).Click();
                //Compose.Click();
            }

        }

        public void CompleteSubject()
        {
            if (Compose.Equals(emailSubject2)) ;
            {
                Compose.Click();
            }
        }

        public void OpenRegistrationEmail()
        {

            bool flag = false;

            IList<IWebElement> allRows = myInbox.FindElements(By.TagName("tr"));
            for (int i = 0; i < allRows.Count; i++)
            {
                for (int j = 0; j < allRows.ElementAt(i).FindElements(By.TagName("td")).Count; j++)
                {
                    string actualText = allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text;
                    if (actualText.Contains(emailSubject1) || actualText.Contains(emailSubject2))
                    {
                        allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(4).Click();
                        flag = true;
                        break;
                    }
                }
                if (flag)
                {
                    break;
                }
            }
        }

        public void CompleteRegistrationMyChooser()
        {
            Thread.Sleep(1000);
            //webDriver.FindElement(By.XPath("//a[contains(@data-saferedirecturl,'telstrahealth')]")).Click();
            //webDriver.FindElement(By.XPath("//a[contains(@data-saferedirecturl,'mychooser')]")).Click();
            webDriver.FindElement(By.XPath("//a[contains(@href,'nonprod.mychooser.com.au/registration/verify')]")).Click();

            

            gmailLogout();
        }

        public void BackToMyChooser()
        {
            webDriver.FindElement(By.XPath("//a[contains(@data-saferedirecturl,'telstrahealth.com/registration/verify')]")).Click();
        }

        public void gmailLogout()
        {
            try
            {
                //webDriver.FindElement(By.CssSelector(".gb_bb")).Click();
                webDriver.FindElement(By.ClassName("_yb_1kz6k")).Click();

                //webDriver.FindElement(By.XPath("//span[contains(@classname,'gb_ab gbii")).Clickspan.gb_3a.gbii
                webDriver.FindElement(By.XPath("//a[contains(@href,'login.yahoo.com/account/logout')]")).Click();

                //webDriver.FindElement(By.XPath("//span[contains(@textContent,'Sign out')")).Click();
                webDriver.SwitchTo().Alert().Accept();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Continue if not confirmation asked");
            }

        }

        //public void GmailLogin()
        //{
        //    //gmailUserName.SendKeys("choicezforme@gmail.com");
        //    //gmailNext.Click();
        //    Thread.Sleep(2000);
        //    gmailPassWord.SendKeys("Grisham1!");
        //    gmailNext.Click();
        //    Thread.Sleep(2000);
        //    webDriver.Navigate().Refresh();

        //}

        //public void DeleteRegistrationEmail()
        //{
        //    webDriver.Navigate().Refresh();

        //    Thread.Sleep(2000);
        //    IList<IWebElement> allRows = myInbox.FindElements(By.TagName("tr"));
        //    for (int i = 0; i < allRows.Count; i++)
        //    {
        //        bool flag = false;
        //        for (int j = 0; j < allRows.ElementAt(i).FindElements(By.TagName("td")).Count; j++)

        //        {
        //            Debug.WriteLine(allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text);
        //            String a = allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text;
        //            if (allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text.Contains("Your NDIS security code"))
        //            {
        //                allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(1).Click();
        //                flag = true;
        //                webDriver.FindElement(By.CssSelector(".ar9")).Click();

        //                break;
        //            }
        //        }
        //        //if (flag)
        //        //{
        //        //    break;
        //        //}
        //    }
        //    //webDriver.FindElement(By.XPath("//div[contains(@data-tooltip,'Delete')]")).Click();
        //    //webDriver.FindElement(By.ClassName("T-Jo-auh")).Click();

        //    //webDriver.FindElement(By.CssSelector(".ar9")).Click();

        //}

        //public void OpenRegistrationEmail()
        //{
        //    webDriver.Navigate().Refresh();

        //    bool flag = false;
        //    IList<IWebElement> allRows = myInbox.FindElements(By.TagName("tr"));
        //    for (int i = 0; i < allRows.Count; i++)
        //    {
        //        for (int j = 0; j < allRows.ElementAt(i).FindElements(By.TagName("td")).Count; j++)
        //        {
        //            //if (allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text.Contains("Registration Confirmation You have successfully completed your registration"))
        //            if (allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(j).Text.Contains("Your NDIS security code"))

        //            {
        //                allRows.ElementAt(i).FindElements(By.TagName("td")).ElementAt(4).Click();
        //                flag = true;
        //                break;
        //            }
        //        }
        //        if (flag)
        //        {
        //            break;
        //        }
        //    }
        //}

        //public void CompleteResitrationMyChooser()
        //{
        //    webDriver.FindElement(By.XPath("//a[contains(@data-saferedirecturl,'telstrahealth')]")).Click();

        //    Thread.Sleep(10000);

        //    webDriver.Navigate().Refresh();
        //    //SignInButton.Click();
        //}







    }
}






