﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Threading;

namespace SeleniumBDD.StepDefinitions
{
    public class SupportEnquiriesPage
    {
        private IWebDriver WebDriver;

        public SupportEnquiriesPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);

        }

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'/servicerequests')]")] private IWebElement Support { get; set; }

        public bool SupportBtn()
        {
            return Support.Enabled;

        }

        public void SupportEnquiries()
        {
            Support.Click();
        }

        public void Include()
        {
            Thread.Sleep(5000);
            WebDriver.FindElement(By.XPath("//input[contains(@type,'checkbox')]")).Click();


        }
    }
}