﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace SeleniumBDD.PageObjects
{
    public class ServiceRequestPage
    {
        private IWebDriver WebDriver;

        public ServiceRequestPage(IWebDriver webDriver)
        {
            this.WebDriver = webDriver;
            PageFactory.InitElements(webDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
        }

        [FindsBy(How = How.Id, Using = "txtSpecialRequirements")] private IWebElement SpecialRequirement { get; set; }
        [FindsBy(How = How.Id, Using = "btnRequestService")] private IWebElement SendEnquiry { get; set; }
        [FindsBy(How = How.Id, Using = "txtContactNo")] private IWebElement PhNo { get; set; }
        [FindsBy(How = How.Id, Using = "txtAdditionalEmail")] private IWebElement AdditionalEMail { get; set; }       
        [FindsBy(How = How.LinkText, Using = "Click here")] private IWebElement ClickHere { get; set; }

        [FindsBy(How = How.Id, Using = "btnCancel")] private IWebElement Cancel { get; set; }
        [FindsBy(How = How.Id, Using = "btnSignIn")] private IWebElement SignIn { get; set; }
        [FindsBy(How = How.ClassName, Using = "provider-button-light-small")] private IWebElement LeaveReview { get; set; }
        [FindsBy(How = How.ClassName, Using = "rating")] private IWebElement Rating { get; set; }
        [FindsBy(How = How.Id, Using = "txtYourComments")] private IWebElement Comments { get; set; }
        [FindsBy(How = How.Id, Using = "butnPreview")] private IWebElement PreviewBtn { get; set; }
        [FindsBy(How = How.Id, Using = "btnSendRating")] private IWebElement btnSendRating { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'/servicerequests')]")] private IWebElement Support { get; set; }

        [FindsBy(How = How.LinkText, Using = "Click here")] private IWebElement ClickSHere { get; set; }
        [FindsBy(How = How.Id, Using = "suburbPostcode")] private IWebElement suburbPostcode { get; set; }
        [FindsBy(How = How.ClassName, Using = "consumer-button-dark-large")] private IWebElement LGAContinue { get; set; }

        

        public void SelectLGA(String LGAName)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(LGAName))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void Suburb()
        {
            //suburbPostcode.SendKeys("MELBOURNE, 3004 VIC");
            //suburbPostcode.SendKeys("RINGWOOD NORTH, 3134 VIC");

            //if (suburbPostcode.Equals("RINGWOOD NORTH, 3134 VIC"))
            //{
            //    Thread.Sleep(1000);
            //    suburbPostcode.SendKeys(Keys.Tab);
            //    suburbPostcode.SendKeys(Keys.Enter);
            //    SelectLGA("Maroondah (C)");
            //    LGAContinue.Click();
            //}

            suburbPostcode.SendKeys("Wolgan Valley");
            Thread.Sleep(1900);
            suburbPostcode.SendKeys(Keys.Tab);
            suburbPostcode.SendKeys(Keys.Enter);

        }

        public void Mychoices()
        {
            Thread.Sleep(5000);
            WebDriver.FindElement(By.ClassName("dropdown-toggle")).Click();
            Support.Click();
        }

        public Boolean SpecialRequirementTxt()
        {
            return SpecialRequirement.Enabled;
        }

        public Boolean SignInB()
        {
            return SignIn.Enabled;
        }

        public void SignInButton()
        {
            SignIn.Click();
        }

        public void SendEnquiryButton()
        {

            PhNo.Clear();
            PhNo.SendKeys("0999999999");
            AdditionalEMail.SendKeys("Choicezforme@gmail.com");
            SpecialRequirement.SendKeys("Special Request");

            Thread.Sleep(2000);
            SendEnquiry.Click();


        }

        public void SelectAgain()
        {
            

        }

        public Boolean ClickHereLinkText()
        {
            return ClickHere.Enabled;
        }

        public void ClickHereLink()
        {
            Boolean visible = false; // assume it is invisible
            try
            {

                IWebElement Subject = WebDriver.FindElements(By.ClassName("yes-no-button")).ElementAt(0);
            
                if (Subject.Displayed)
                    visible = true;

                Subject.Click();
            }
            catch (Exception e)
            {
                //Assert.IsFalse(visible);
            }

            ClickHere.Click();
        }

        public bool ConfirmSupportEnquiry()
        {
            Thread.Sleep(2000);
            //return WebDriver.FindElement(By.ClassName("col-md-12.page-header1")).GetAttribute("innerText").Trim().Equals("Support Enquiries");
            //return WebDriver.FindElement(By.XPath("//h1(contains[@class,'col-md-12.page-header1'])")).GetAttribute("innerText").Trim().Equals("Support Enquries");

            //List<IWebElement> allcheckbox = WebDriver.FindElements(By.XPath("//input[contains(@type,'checkbox')]"));

            //for (IWebElement element : allcheckbox)
            //{
            //    if (!(element.Selected()))
            //    {
            //        element.Click();
            //    }
            //}
            return LeaveReview.Enabled;
        }

        public Boolean CancelButton()
        {
            return Cancel.Enabled;
        }

        public void LeaveaReview()
        {
            //LeaveReview.Click();
            WebDriver.FindElements(By.ClassName("provider-button-light-small")).ElementAt(0).Click();
        }

        public void RateRating()
        {
            //WebDriver.FindElements(By.ClassName("radio")).ElementAt(4).Click();
            IList<IWebElement> Rate = WebDriver.FindElements(By.ClassName("rating"));

            // This will tell you the number of Radio buttons are present
            int Size = Rate.Count;

            // Start the loop from first Radio button to last Radio button
            for (int i = 0; i < Size; i++)
            {
                // Store the Radio button name to the string variable, using 'Value' attribute
                String Value = Rate.ElementAt(i).GetAttribute("value");

                // Select the checkbox it the value of the checkbox is same what you are looking for
                if (Value.Equals("1_rating"))
                {
                    Rate.ElementAt(i).Click();
                    // This will take the execution out of for loop
                    break;
                }
                //    IWebElement Ratings = WebDriver.FindElement(By.ClassName("rating"));
                //    IWebElement Ratngs = WebDriver.FindElement(By.Name("1_ratings"));

                //Actions action = new Actions(WebDriver);
                //    action.MoveToElement(Ratings).Click(Ratngs).Build().Perform();   
            }
        }

        public Boolean GiveComment()
        {
            return Comments.Enabled;
        }

        public void GiveComments()
        {
            Comments.SendKeys("Service Provided");

            Thread.Sleep(4000);
            //WebDriver.FindElements(By.ClassName("rating")).ElementAt(4).Click();
            //WebDriver.FindElements(By.Name("1_rating")).ElementAt(4).Click();
            //WebDriver.FindElement(By.CssSelector("//input[@value='4']")).Click();
           // WebDriver.FindElement(By.XPath("//label(contains[@_ngcontent-c23, '5 stars'])"));
            PreviewBtn.Click();
        }

        public Boolean BtnSendRatings()
        {
            return btnSendRating.Enabled;
        }

        public void SendRating()
        {
            btnSendRating.Click();
        }

        public bool ConfirmMyReviews()
        {
            return WebDriver.FindElement(By.ClassName("col-md-12.page-header1")).GetAttribute("innerText").Trim().Equals("My Reviews");

        }

        public Boolean ClicksHereLinkText()
        {
            return ClickSHere.Enabled;
        }

        public void ClicksHereLink()
        {
            ClickSHere.Click();
        }


    }
        

    
}
   