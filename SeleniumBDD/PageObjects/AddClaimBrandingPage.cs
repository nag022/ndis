﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class AddClaimBrandingPage
    {
        public IWebDriver WebDriver;
        public AddClaimBrandingPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            //WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

        }
        [FindsBy(How = How.Id, Using = "btnSetupProfile")] private IWebElement SetUpProfile { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'legalEntityName')]")] private IWebElement LegalEntityName { get; set; }
        [FindsBy(How = How.Id, Using = "txtAcn")] private IWebElement ACN { get; set; }
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Organization.extension:comment.valueString')]")] private IWebElement Description { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.telecom:website.value')]")] private IWebElement WebSite { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.extension:ndis-reg-status.valueCoding')]")] private IWebElement NDISregistered { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement BtnSave { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddLocation")] private IWebElement AddLocation { get; set; }
        [FindsBy(How = How.Id, Using = "txtProviderNumber")] private IWebElement ProviderNumber { get; set; }
        [FindsBy(How = How.Id, Using = "txtPhoneNo")] private IWebElement PhNumberObj { get; set; }



        public void AddClaimOrganizationDetails()
        {
            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            //WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(5).Click();

            Thread.Sleep(5000);
            //Add Logo
            //WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            //System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");
            ACN.SendKeys("123456789");

            LegalEntityName.Clear();
            LegalEntityName.SendKeys("provider Pty Ltd");

            
            Thread.Sleep(5000);
            //add description          
            Description.SendKeys("NDIS provider");
            //WebDriver.FindElements(By.XPath("//input[contains(@name,'Organization.extension:ndis-reg-status.valueCoding')]")).ElementAt(0).Click();

            WebDriver.FindElements(By.Id("lblReg")).ElementAt(0).Click();

            ProviderNumber.SendKeys("999999999");

            //PhNumberObj.SendKeys("0999999999");
            Thread.Sleep(5000);
            //Add Banner
            //WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(1).Click();
            //System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");
            //BtnSave.Click();
            WebSite.Clear();

        }

        public void SubmitClaimOrganizationDetails()
        {
            Thread.Sleep(5000);
            BtnSave.Click();
        }

    }
}


