﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class UserRegistrationPage
    {
        public IWebDriver WebDriver;
        public String PhNo = "123456" + GeneralUtility.randumNumber();

        public String Name = "John" + " " + GeneralUtility.RandomAlphaNumeric();
        //public String Email = "marketplacendis+" + GeneralUtility.randumNumber() + "@gmail.com";

        public String Email;


        public UserRegistrationPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);
            Email = ConfigurationManager.AppSettings["GmailUserName"] + "+" + GeneralUtility.randumNumber() + "@gmail.com";
            //Email = "choicezforme" + "+" + GeneralUtility.randumNumber() + "@yahoo.com";
            //Email = "marketplacendis+" + GeneralUtility.randumNumber() + "@gmail.com";

        }


        [FindsBy(How = How.Id, Using = "consumer-sign-up-button")] private IWebElement RegisterObj { get; set; }
        [FindsBy(How = How.Id, Using = "txtFirstName")] private IWebElement FirstNameObj { get; set; }
        [FindsBy(How = How.Id, Using = "txtLastName")] private IWebElement LastNameObj { get; set; }
        [FindsBy(How = How.Id, Using = "txtSuburb")] private IWebElement SubUrbObj { get; set; }
        [FindsBy(How = How.Id, Using = "txtPhoneNo")] private IWebElement PhNumberObj { get; set; }
        [FindsBy(How = How.Id, Using = "txtEmail")] private IWebElement EmailObj { get; set; }
        [FindsBy(How = How.CssSelector, Using = "#btnRegister")] private IWebElement SubmitRegisterObj { get; set; }
        [FindsBy(How = How.Id, Using = "agreeTerms")] private IWebElement AgreeTerms { get; set; }

        
        [FindsBy(How = How.Id, Using = "txtConfirmPassword")] private IWebElement ConfirmPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'/signin')]")] private IWebElement SignInButton { get; set; }
        [FindsBy(How = How.Id, Using = "txtPassword")] private IWebElement Password { get; set; }
        [FindsBy(How = How.Id, Using = "btnSignIn")] private IWebElement LoginButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'searchorganisation')]")] private IWebElement ProviderSearch { get; set; }

        public Boolean ConfirmNDIShomePage()
        {
            return RegisterObj.Enabled;
        }
        public void CallRegisterButtn()
        {
            RegisterObj.Click();
        }

        public Boolean ConfirmRegistrationForm()
        {
            return FirstNameObj.Enabled;
        }

        public void FillRegistrationForm()
        {
            WebDriver.FindElement(By.Id("txtFirstName")).SendKeys(Name);
            WebDriver.FindElement(By.Id("txtLastName")).SendKeys("Grisham");
            //Thread.Sleep(10000);
            Thread.Sleep(8000);

            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);


            WebDriver.FindElement(By.Id("txtSuburb")).SendKeys("MELBOURNE, 3004 VIC");
            //SubUrbObj.SendKeys(Keys.ArrowDown);
            Thread.Sleep(1000);
            SubUrbObj.SendKeys(Keys.Tab);

            //WebDriver.Navigate().Refresh();
            //WebDriver.FindElement(By.Id("txtFirstName")).SendKeys("John");
            //WebDriver.FindElement(By.Id("txtLastName")).SendKeys("Grisham");
            //SubUrbObj.Clear();
            //WebDriver.FindElement(By.Id("txtSuburb")).SendKeys("MELBOURNE, 3004 VIC");

            // myAction.SendKeys(Keys.ArrowDown).Build().Perform();
            // myAction.SendKeys(Keys.Enter).Build().Perform();
            // myAction.SendKeys(Keys.Tab).Build().Perform();
            // WebDriver.FindElement(By.Id("txtSuburb")).SendKeys("PARRAMATTA, 2123 NSW");
            //PhNumberObj.SendKeys(PhNo);

            WebDriver.FindElement(By.Id("txtEmail")).SendKeys(Email);

            AgreeTerms.Click();

            //Screenshot ss = ((ITakesScreenshot)WebDriver).GetScreenshot();
            //ss.SaveAsFile("C:\\Logos", ImageFormat.Jpeg);

            //WebDriver.TakeScreenshot().SaveAsFile("C:\\Logos", ScreenshotImageFormat.Jpeg);


            // WebDriver.FindElement(By.Id("txtEmail")).SendKeys(ConfigurationManager.AppSettings["GmailUserName"]+ GeneralUtility.randumNumber() + "@gmail.com");
            // WebDriver.FindElement(By.Id("txtEmail")).SendKeys(ConfigurationManager.AppSettings["GmailUserName"] + GeneralUtility.randumNumber() + "@gmail.com");
            WebDriver.FindElement(By.Id("btnRegister")).SendKeys(Keys.Enter);
            Thread.Sleep(2000);
        }

        public void EmailORG()
        {
            WebDriver.FindElement(By.Id("txtEmail")).Clear();
            WebDriver.FindElement(By.Id("txtEmail")).SendKeys(Email);

        }

      
            
        


        //public void FillRegistrationForm()
        //{
        //    FirstNameObj.SendKeys(ConfigurationManager.AppSettings["firstName"]);
        //    LastNameObj.SendKeys(ConfigurationManager.AppSettings["lastName"]);
        //    //SubUrbObj.SendKeys(ConfigurationManager.AppSettings["subUrb"]);
        //    //Thread.Sleep(15000);
        //    WebDriver.FindElement(By.Id("txtSuburb")).SendKeys("PARRAMATTA, 2123 NSW");

        //   //SubUrbObj.SendKeys("MELBERGEN");
        //    Thread.Sleep(15000);
        //    SubUrbObj.Clear();
        //    WebDriver.FindElement(By.Id("txtSuburb")).SendKeys("PARRAMATTA, 2123 NSW");

        //    //SubUrbObj.SendKeys(ConfigurationManager.AppSettings["subUrb"]);

        //    //SubUrbObj.SendKeys(Keys.ArrowDown);
        //    //SubUrbObj.SendKeys(Keys.Enter);

        //    //WebDriver.FindElement(By.Id("txtSuburb")).SendKeys(Keys.ArrowDown);
        //    //WebDriver.FindElement(By.Id("txtSuburb")).SendKeys(Keys.Enter);

        //    //PhNumberObj.SendKeys(ConfigurationManager.AppSettings["phNumber"]);
        //    PhNumberObj.SendKeys(PhNo);

        //    //EmailObj.SendKeys(ConfigurationManager.AppSettings["email"]);
        //    EmailObj.SendKeys(Email);
        //    WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(400);
        //    SubmitRegisterObj.Click();

        //}

        public Boolean ConfirmThankYouMessage()
        {
            //String successMsg = WebDriver.FindElements(By.ClassName("main-content-text")).ElementAt(0).GetAttribute("innerText");
            String successMsg = WebDriver.FindElements(By.ClassName("col-md-12 ")).ElementAt(0).GetAttribute("innerText");

            return (successMsg.Contains("Thank you for registering with My Life, My Choices"));
        }



        //public Boolean VerifyDetails()
        //{
        //    bool found = Password.Displayed;
        //    Thread.Sleep(250);
        //    return (found && Password.Enabled));
        //    //return Password.Enabled;
        //    //return WebDriver.FindElement(By.XPath("//input[contains(@id,'txtPassword')]")).Enabled;

        //}

        public void ChoosePassword()
        {
            WebDriver.Navigate().Refresh();

            WebDriver.SwitchTo().Window(WebDriver.WindowHandles.ElementAt(1));
            //WebDriver.Navigate().Refresh();
            //SignInButton.Click();
            Password.SendKeys(ConfigurationManager.AppSettings["password"]);
            ConfirmPassword.SendKeys(ConfigurationManager.AppSettings["password"]);
            Thread.Sleep(5000);

            //WebDriver.FindElement(By.Id("txtPassword")).SendKeys("Grisham1!");
            //WebDriver.FindElement(By.Id("txtConfirmPassword")).SendKeys("Grisham1!");
            WebDriver.FindElement(By.Id("btnRegister")).Click();

        }

        public Boolean ConfirmMessage()
        {
            //String successMsg = WebDriver.FindElements(By.ClassName("main-content-text")).ElementAt(0).GetAttribute("innerText");
            String successMsg = WebDriver.FindElements(By.ClassName("col-md-12 ")).ElementAt(0).GetAttribute("innerText");

            return (successMsg.Contains("Congratulations on successfully completing the registration process."));
        }

        public void LinkText()
        {
            Thread.Sleep(5000);
            //WebDriver.FindElement(By.LinkText("Sign in")).Click();
            SignInButton.Click();
        }

        public Boolean ConfirmRegistration()
        {
            return EmailObj.Enabled;
        }

        public void SigninToApplication()
        {
            Thread.Sleep(5000);
            //SignInButton.Click();
            EmailObj.SendKeys(Email);
            Password.SendKeys(ConfigurationManager.AppSettings["password"]);
            //WebDriver.Navigate().Refresh();
            Thread.Sleep(2000);
            //EmailObj.SendKeys(Email);
            //Password.SendKeys(ConfigurationManager.AppSettings["password"]);
            LoginButton.Click();

        }

       
    }

}
