﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumBDD.PageObjects
{
    public class AddBrandingPage
    {
        public IWebDriver WebDriver;
        public AddBrandingPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            //WebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

        }
        [FindsBy(How = How.Id, Using = "btnSetupProfile")] private IWebElement SetUpProfile { get; set; }
      
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Organization.extension:comment.valueString')]")] private IWebElement Description { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.telecom:website.value')]")] private IWebElement WebSite { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Organization.extension:ndis-reg-status.valueCoding')]")] private IWebElement NDISregistered { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement BtnSave { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddLocation")] private IWebElement AddLocation { get; set; }




        public Boolean SetUpMyProfileOption()
        {
            return SetUpProfile.Enabled;
        }

        public void SelectSetUpMyProfileOption()
        {
            Thread.Sleep(4000);
            WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(4).Click();
        }

        public Boolean ConfirmClick()
        {
            return Description.Enabled;

        }

        public void AddOrganizationDetails()
        {
            //WebDriver.FindElement(By.Id("btnSetupProfile")).SendKeys(Keys.Enter);
            //WebDriver.FindElements(By.ClassName("provider-button-dark-large")).ElementAt(5).Click();

            Thread.Sleep(5000);
            //Add Logo
            //WebDriver.FindElement(By.ClassName("fileupload")).Click();

            WebDriver.FindElement(By.XPath("//label[contains(@class,'fileupload')]")).Click();
            System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");

            Thread.Sleep(5000);
            //add description          
            Description.SendKeys("NDIS provider");
            WebDriver.FindElements(By.XPath("//input[contains(@name,'Organization.extension:ndis-reg-status.valueCoding')]")).ElementAt(0).Click();

            Thread.Sleep(5000);
            //Add Banner
            WebDriver.FindElement(By.XPath("//label[contains(@class,'fileupload')]")).Click();
            System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");
            //BtnSave.Click();
            WebSite.Clear();

        }


        //public void AddOrganizationDetails()

        //{
        //    Description.SendKeys(ConfigurationManager.AppSettings["Description"]);
        //    //private IWebElement radioBtn = driver.FindElement(By.Id("4d28df4e-82bc-41cf-b0aa-bab4b59ff934"));

        //    //radioBtn.Click();
        //    NDISregistered.Click();

        //}

        //public Boolean OrganizationNotAvailableInDataBase()
        //{
        //    Debug.WriteLine("“Searched Organization SearchOrgName isn't on Choices for me yet” ");
        //    return NewProviderRegister.Enabled;
        //}

        //public Boolean RegisterNewOrganizationOption()
        //{
        //    return NewProviderRegister.Enabled;
        //}
        //public void SelectRegisterOrganization()
        //{
        //    NewProviderRegister.Click();
        //}
        //public Boolean ConfirmRegistrationPage()
        //{
        //    return TradingName.Enabled;
        //}

        //public void EnterRegistrationForm()
        //{
        //    TradingName.SendKeys(ConfigurationManager.AppSettings["OrgName"]);
        //    LegalEntityName.SendKeys(ConfigurationManager.AppSettings["orgLegalEntity"]);
        //    ABN.SendKeys(ConfigurationManager.AppSettings["Org ABN"]);
        //    HeadOfficeAddress.SendKeys(ConfigurationManager.AppSettings["Org Address"]);
        //    PostCode.SendKeys(ConfigurationManager.AppSettings["postalCode1234"]);
        //    WebSite.SendKeys(ConfigurationManager.AppSettings["abc"]);
        //    //TradingName.SendKeys("OrgName");
        //    //LegalEntityName.SendKeys("orgLegalEntity");
        //    //ABN.SendKeys("Org ABN");
        //    //HeadOfficeAddress.SendKeys("Org Address");
        //    //PostCode.SendKeys("postalCode1234");
        //    //WebSite.SendKeys("abc");
        //}


        public void SubmitOrganizationDetails()
        {
            Thread.Sleep(5000);
            BtnSave.Click();
        }

        public Boolean ConfirmAddBranding()
        {
            return AddLocation.Enabled;
        }

        public Boolean AddLocationOption()
        {
            return AddLocation.Enabled;
        }

    }
}


