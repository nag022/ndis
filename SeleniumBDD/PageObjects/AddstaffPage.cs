﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace SeleniumBDD.StepDefinitions
{
    public class AddStaffPage
    {
        public IWebDriver WebDriver;

        public AddStaffPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
        }


        [FindsBy(How = How.Id, Using = "btnStaff")] public IWebElement AddStaffButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnDoThisLater")] public IWebElement DoThisLaterButton { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.name.given')]")] private IWebElement PractitionerGivenName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.name.family')]")] private IWebElement PractitionerFamilyName { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@data-fhirq-question-link-id,'Practitioner.practitionerRole.role.text')]")] private IWebElement PractitionerRole { get; set; }
        [FindsBy(How = How.XPath, Using = "//textarea[contains(@data-fhirq-question-link-id,'Practitioner.extension:bio.valueString')]")] private IWebElement PractitionerBio { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddDifferentQualification")] private IWebElement AddDifferentQualification { get; set; }
        [FindsBy(How = How.Id, Using = "txtAddQualification")] private IWebElement PractitionerQualification { get; set; }

        [FindsBy(How = How.Id, Using = "btnAddQualification")] private IWebElement AddQualification { get; set; }
        [FindsBy(How = How.Id, Using = "btnChooseServices")] private IWebElement ChooseServices { get; set; }
        [FindsBy(How = How.Id, Using = "btnSaveService")] private IWebElement SaveService { get; set; }
        [FindsBy(How = How.Id, Using = "btnSave")] private IWebElement SaveStaff { get; set; }
        [FindsBy(How = How.Id, Using = "btnAddAnotherPerson")] private IWebElement AddAnotherPerson { get; set; }
        [FindsBy(How = How.Id, Using = "btnFinish")] private IWebElement Finish { get; set; }
        [FindsBy(How = How.Id, Using = "btnViewYourProfile")] private IWebElement ViewYourProfile { get; set; }

        
        public void SelectAddStaff()
        {
            AddStaffButton.Click();
        }

        public Boolean AddStaff()
        {
            //Thread.Sleep(4000);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            return PractitionerGivenName.Enabled;
        }

        public void EnterSupportWorkerDetails()
        {
            PractitionerGivenName.SendKeys("Adam");
            PractitionerFamilyName.SendKeys("Gilchrist");
            PractitionerRole.SendKeys("Support Worker");



            //SelectElement mydropDown = new SelectElement(WebDriver.FindElement(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")));
            //mydropDown.SelectByIndex(1);

            //SelectElement mydropDown = new SelectElement(WebDriver.FindElement(By.ClassName("fhirq-select")));
            //mydropDown.SelectByValue("Male");


            //WebDriver.FindElements(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")).ElementAt(1).Click();


            PractitionerBio.SendKeys("Experienced");

            //Thread.Sleep(2000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            //WebDriver.FindElement(By.ClassName("fileupload"));
            ////WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            //System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");

            Thread.Sleep(5000);

            //WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();

            WebDriver.FindElement(By.Id("chkQualifications_0")).Click();

            WebDriver.FindElement(By.Id("chkQualifications_1")).Click();

            AddDifferentQualification.Click();
            PractitionerQualification.SendKeys("Safety Hazard Level 4");
            AddQualification.Click();

            Thread.Sleep(5000);
            //WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();

            
            Thread.Sleep(2000);
            //WebDriver.FindElement(By.Id("c99e9729f3604ed7ba98a5cfdb77c57a")).Click();
            WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(3).Click();
            WebDriver.FindElement(By.Id("AHPRANumber")).SendKeys("999999999");

            ChooseServices.Click();
            //AssitanceWithDailyLiving("Accommodation/Tenancy");

            Thread.Sleep(4000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            //WebDriver.FindElements(By.Id("chk0101")).ElementAt(0).Click();
            if (WebDriver.FindElements(By.Name("chkService")).Count > 0)
                WebDriver.FindElements(By.Name("chkService")).ElementAt(0).Click();
                WebDriver.FindElements(By.Name("chkService")).ElementAt(1).Click();
                WebDriver.FindElements(By.Name("chkService")).ElementAt(3).Click();
                WebDriver.FindElements(By.Name("chkService")).ElementAt(4).Click();

            Thread.Sleep(2000);
            SaveService.Click();

            SaveStaff.Click();

        }

        public void EnterAnotherSupportWorkerDetails()
        {
            PractitionerGivenName.SendKeys("John");
            PractitionerFamilyName.SendKeys("Grisham");
            PractitionerRole.SendKeys("Support Worker");



            SelectElement mydropDown = new SelectElement(WebDriver.FindElement(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")));
            mydropDown.SelectByIndex(1);

            //SelectElement mydropDown = new SelectElement(WebDriver.FindElement(By.ClassName("fhirq-select")));
            //mydropDown.SelectByValue("Male");


            //WebDriver.FindElements(By.XPath("//input[contains(@data-fhirq-question-link-id,'Practitioner.gender')]")).ElementAt(1).Click();


            PractitionerBio.SendKeys("Experienced");

            Thread.Sleep(2000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            System.Diagnostics.Process.Start(@"C:\Users\2928\Source\Repos\NDIS Marketplace\SeleniumBDD\SeleniumBDD\DataFiles\UploadLogo.exe");

            Thread.Sleep(5000);

            //WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();

            WebDriver.FindElement(By.Id("chkQualifications_1")).Click();

            AddDifferentQualification.Click();
            PractitionerQualification.SendKeys("Safety Hazard Level 4");
            AddQualification.Click();

            WebDriver.FindElements(By.ClassName("checkbox")).ElementAt(0).Click();

            //WebDriver.FindElements(By.Id("allLocations")).ElementAt(0).Click();

            ChooseServices.Click();
            //AssitanceWithDailyLiving("Accommodation/Tenancy");

            Thread.Sleep(4000);
            //WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            //WebDriver.FindElements(By.Id("chk0101")).ElementAt(0).Click();
            if (WebDriver.FindElements(By.Name("chkService")).Count > 0)
                WebDriver.FindElements(By.Name("chkService")).ElementAt(0).Click();

            Thread.Sleep(2000);
            SaveService.Click();

            SaveStaff.Click();

        }

        //public void AssitanceWithDailyLiving(String assitanceName)
        //{
        //    ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("chkService"));
        //    for (int i = 0; i < allAssistance.Count; i++)
        //    {
        //        if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(assitanceName))
        //        {
        //            allAssistance.ElementAt(i).Click();
        //            break;
        //        }
        //    }
        //}

        public Boolean AddAnotherStaffButton()
        {
            return AddAnotherPerson.Enabled;
        }

        public Boolean FinishButton()
        {
            return Finish.Enabled;
        }

        public void SelectFinish()
        {
            Finish.Click();
        }

        public Boolean Done()
        {
            return ViewYourProfile.Enabled;
        }

        public void SelectViewYourProfile()
        {
            ViewYourProfile.Click();
        }

    }
}