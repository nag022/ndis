﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using SeleniumBDD.Utilities;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace SeleniumBDD.StepDefinitions
{
    public class AddServicesPage
    {
        public IWebDriver WebDriver;

        public AddServicesPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

        }

        [FindsBy(How = How.Id, Using = "btnAddService")] private IWebElement BtnAddService { get; set; }

        [FindsBy(How = How.Id, Using = "btnSupportCategoryContinue")] public IWebElement CategoryContinue { get; set; }
        [FindsBy(How = How.Id, Using = "btnSubmit")] public IWebElement SubmitButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnStaff")] public IWebElement AddStaffButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnSubmit")] public IWebElement ContinueButton { get; set; }
        [FindsBy(How = How.Id, Using = "btnDoThisLater")] public IWebElement DoThisLaterButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'home')]")] private IWebElement Home { get; set; }


        public void SelectAddServices()
        {
            BtnAddService.Click();

        }

        public Boolean SupportCategories()
        {
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            return CategoryContinue.Enabled;      

        }

        public void AddSupportCategories()
        {
            //AddServiceButton.Click();
            SelectService("Assistance With Daily Living");
            //SelectService("Home Modifications");
            //SelectService("Improved Living Arrangements");
            //SelectService("Improved Relationships");
            //SelectService("Improved Life Choices");
            SelectService("Transport");
            SelectService("Consumables");
            CategoryContinue.Click();
        }

        public Boolean RegisteredGroups()
        {
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            return SubmitButton.Enabled;

        }


        //public void SelectAssitanceWithDailyLiving()


        public void AddRegisteredGroups()
        {
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

            //AssitanceWithDailyLiving("Accommodation/Tenancy");
            //WebDriver.FindElements(By.Id("0101")).ElementAt(0).Click();
            //AssitanceWithDailyLiving("Yes");
            //AssitanceWithDailyLiving("Bourke Place");
            //WebDriver.FindElements(By.CssSelector(".button-inside-label")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Id("0101")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Id("chkNDISRegistered0010101")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Name("0010101")).ElementAt(0).Click();


            //WebDriver.FindElements(By.Id("0120")).ElementAt(0).Click();
            //AssitanceWithDailyLiving("No, we only provide this to self-managed participants");
            //AssitanceWithDailyLiving("Bourke Place");

            Thread.Sleep(5000);
            Home.SendKeys(Keys.Home);
            Thread.Sleep(2000);

            AssitanceWithDailyLiving("Accommodation/Tenancy");
            WebDriver.FindElements(By.Id("chkNDISRegistered0010101")).ElementAt(0).Click();
            WebDriver.FindElements(By.Name("0010101")).ElementAt(1).Click();



            AssitanceWithDailyLiving("Daily Personal Activities");
            Thread.Sleep(2000);
            WebDriver.FindElements(By.Id("chkNDISRegistered0010107")).ElementAt(0).Click();
            WebDriver.FindElements(By.Name("0010107")).ElementAt(1).Click();


            AssitanceWithDailyLiving("High Intensity Daily Personal Activities");
            Thread.Sleep(2000);
            WebDriver.FindElements(By.Id("chkNDISRegistered0010104")).ElementAt(0).Click();
            WebDriver.FindElements(By.Name("0010104")).ElementAt(1).Click();


            AssitanceWithDailyLiving("Household Tasks");
            Thread.Sleep(2000);
            WebDriver.FindElements(By.Id("chkNDISRegistered0010120")).ElementAt(1).Click();
            WebDriver.FindElements(By.Name("0010120")).ElementAt(1).Click();

            Thread.Sleep(2000);

            SubmitButton.Click();

            Home.SendKeys(Keys.Home);
            Thread.Sleep(2000);
            chooseService("Assistance with Travel and/or Transport");
            Thread.Sleep(2000);
            WebDriver.FindElements(By.Id("chkNDISRegistered0020108")).ElementAt(1).Click();
            WebDriver.FindElements(By.Name("0020108")).ElementAt(1).Click();

            SubmitButton.Click();

            Thread.Sleep(2000);
            Home.SendKeys(Keys.Home);
            Thread.Sleep(2000);

            chooseService("Assistive products for personal care and safety");
            Thread.Sleep(2000);
            WebDriver.FindElements(By.Id("chkNDISRegistered0030103")).ElementAt(1).Click();
            WebDriver.FindElements(By.Name("0030103")).ElementAt(1 ).Click();




            //DoThisLaterButton.Click();

            //ContinueButton.Click();
        }

        public void selectAssitanceWithDailyLiving_edit()
        {
            
            Thread.Sleep(2000);
            chooseService("Accommodation/Tenancy");
            WebDriver.FindElements(By.Id("chkNDISRegistered0010101")).ElementAt(0).Click();
            //WebDriver.FindElements(By.Id("dcf988d751e3439f83509b529597305f0010101")).ElementAt(0).Click();
            WebDriver.FindElement(By.Id("dcf988d751e3439f83509b529597305f0010101")).Click();



            chooseService("Daily Personal Activities");
            WebDriver.FindElements(By.Id("chkNDISRegistered0010107")).ElementAt(0).Click();
            WebDriver.FindElements(By.Id("dcf988d751e3439f83509b529597305f0010107")).ElementAt(0).Click();


            chooseService("High Intensity Daily Personal Activities");
            WebDriver.FindElements(By.Id("chkNDISRegistered0010104")).ElementAt(0).Click();
            WebDriver.FindElements(By.Id("dcf988d751e3439f83509b529597305f0010104")).ElementAt(0).Click();


            chooseService("Household Tasks");
            WebDriver.FindElements(By.Id("chkNDISRegistered0010120")).ElementAt(1).Click();
            WebDriver.FindElements(By.Id("dcf988d751e3439f83509b529597305f0010120")).ElementAt(0).Click();


            SubmitButton.Click();

            chooseService("Assistance with Travel and/or Transport");
            WebDriver.FindElements(By.Id("chkNDISRegistered0020108")).ElementAt(1).Click();
            WebDriver.FindElements(By.Id("dcf988d751e3439f83509b529597305f0020108")).ElementAt(0).Click();

            SubmitButton.Click();

            chooseService("Assistive products for personal care and safety");
            WebDriver.FindElements(By.Id("chkNDISRegistered0030103")).ElementAt(1).Click();
            WebDriver.FindElements(By.Id("dcf988d751e3439f83509b529597305f0030103")).ElementAt(0).Click();


            //DoThisLaterButton.Click();
        }

        public void SelectContinue()
        {
            SubmitButton.Click();

        }

        public void chooseService(String serviceName)
        {
            //ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("pt15px"));
            for (int i = 0; i < allAssistance.Count; i++)
            {
                if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allAssistance.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void SelectService(String serviceName)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }

        public void AssitanceWithDailyLiving(String assitanceName)
        {
            ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("pt15px"));
            for (int i = 0; i < allAssistance.Count; i++)
            {
                if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(assitanceName))
                {
                    allAssistance.ElementAt(i).Click();
                    break;
                }
            }
        }

        //public void HomeModifications(String assitanceName)
        //{
        //    ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
        //    for (int i = 0; i < allAssistance.Count; i++)
        //    {
        //        if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(assitanceName))
        //        {
        //            allAssistance.ElementAt(i).Click();
        //            break;
        //        }
        //    }
        //}

        //public void ImprovedLivingArrangements(String assitanceName)
        //{
        //    ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
        //    for (int i = 0; i < allAssistance.Count; i++)
        //    {
        //        if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(assitanceName))
        //        {
        //            allAssistance.ElementAt(i).Click();
        //            break;
        //        }
        //    }
        //}

        //public void ImprovedRelationships(String assitanceName)
        //{
        //    ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
        //    for (int i = 0; i < allAssistance.Count; i++)
        //    {
        //        if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(assitanceName))
        //        {
        //            allAssistance.ElementAt(i).Click();
        //            break;
        //        }
        //    }
        //}

        //public void ImprovedLifeChoices(String assitanceName)
        //{
        //    ReadOnlyCollection<IWebElement> allAssistance = WebDriver.FindElements(By.ClassName("rg-service-display"));
        //    for (int i = 0; i < allAssistance.Count; i++)
        //    {
        //        if (allAssistance.ElementAt(i).GetAttribute("innerText").Contains(assitanceName))
        //        {
        //            allAssistance.ElementAt(i).Click();
        //            break;
        //        }
        //    }
        //}

        public Boolean AddStaffbutton()
        {
            return AddStaffButton.Enabled;
        }

        public Boolean DoitlaterLinkText()
        {
            return DoThisLaterButton.Enabled;
        }

        public void SelectDoitlaterLinkText()
        {
            DoThisLaterButton.Click();
        }

        //public bool ConfirmOrganizationName()
        //{
        //    return WebDriver.FindElement(By.ClassName("page-header1")).GetAttribute("innerText").Trim().Equals(OrgName);
        //}
    }
}
