﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace SeleniumBDD.PageObjects
{
    public class DevOpsTestPage
    {
        public IWebDriver WebDriver;
        Actions myactions;
        public string Orgname = "John";

        public DevOpsTestPage(IWebDriver WebDriver)
        {
            this.WebDriver = WebDriver;
            myactions = new Actions(WebDriver);
            PageFactory.InitElements(WebDriver, this);
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
        }

        [FindsBy(How = How.Id, Using = "suburbPostcode")] private IWebElement Suburb { get; set; }
        [FindsBy(How = How.Id, Using = "txtBusinessName")] private IWebElement GiveProviderName { get; set; }
        [FindsBy(How = How.Id, Using = "btnSearch")] private IWebElement SubmitSearch { get; set; }
        [FindsBy(How = How.Id, Using = "findServicesBtn")] private IWebElement SuburbSearch { get; set; }
        [FindsBy(How = How.Id, Using = "btnSupportCategoryContinue")] public IWebElement CategoryContinue { get; set; }
        [FindsBy(How = How.ClassName, Using = "consumer-button-dark-large")] private IWebElement SavePlan { get; set; }
        [FindsBy(How = How.Id, Using = "chkNdisRegistered")] public IWebElement NDIScheckbox { get; set; }
        //[FindsBy(How = How.ClassName, Using = "row.count-validation-error-div")] public IWebElement Subject1 { get; set; }


        public void SearchOrganization()
        {
            GiveProviderName.SendKeys(Orgname);
            SubmitSearch.Click();
        }

        public void SearchSuburb()
        {
            Thread.Sleep(5000);
            Suburb.SendKeys("Wolgan Valley");
            Suburb.SendKeys(Keys.ArrowDown);
            Thread.Sleep(900);
            Suburb.SendKeys(Keys.Tab);
            Suburb.SendKeys(Keys.Enter);
        }

        public Boolean SupportCategoryPage()
        {
            return CategoryContinue.Enabled;
        }



        public bool ConfirminnerText()
        {
            return WebDriver.FindElement(By.ClassName("row.count-validation-error-div")).GetAttribute("innerText").Trim().Equals("There is a service requested and removing this Support Category from your plan will not delete the Support Enquiry.↵Are you sure you want to proceed?");

        }


        public void SelectSupportCategoy()
        {
            SelectService("Assistance With Daily Living");
            

            Boolean visible = false; // assume it is invisible
            try
            {
                
                IWebElement Subject = WebDriver.FindElements(By.ClassName("count-yes-no-button")).ElementAt(1);
                // if I get to here the element exists
                // if it is visible
                if (Subject.Displayed)
                    visible = true;

                Subject.Click();
            }
            catch (Exception e)
            {
                Assert.IsFalse(visible);
            }
            

            //SelectService("Transport");
            CategoryContinue.Click();
        }

        public void SelectService(String serviceName)
        {
            ReadOnlyCollection<IWebElement> allServices = WebDriver.FindElements(By.ClassName("support-category-title"));
            for (int i = 0; i < allServices.Count; i++)
            {
                if (allServices.ElementAt(i).GetAttribute("innerText").Contains(serviceName))
                {
                    allServices.ElementAt(i).Click();
                    break;
                }
            }
        }

        public Boolean ViewProviderOption()
        {
            //return WebDriver.FindElements(By.ClassName("consumer-button-dark-small")).ElementAt(0).Enabled;
            return SavePlan.Enabled;
        }

        public void SelectViewProviders()
        {
            WebDriver.FindElements(By.ClassName("consumer-button-dark-small")).ElementAt(0).Click();
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

        }

        public Boolean Checkbox()
        {
            return NDIScheckbox.Enabled;
        }

        public void SelectProvider()
        {
            //if (WebDriver.FindElement(By.XPath("//a[contains(@href,'mailto:marketplacendis+2019@gmail.com')]")).Enabled)
            //{
            //    WebDriver.FindElements(By.ClassName("tile-action-button")).ElementAt(4).Click();

            //}
            WebDriver.FindElements(By.ClassName("tile-action-button")).ElementAt(4).Click();
        }

        public bool ConfirmProviderPage()
        {
            return WebDriver.FindElement(By.ClassName("page-header1")).GetAttribute("innerText").Trim().Equals("Provider Details");

        }
    }
}