﻿Feature: MyReviews

@mytag
Scenario: Reviews and Ratings 
	Given As a registered user launch “My Chooser” website
	When  User Sign-In to Participant account where Participant has already requested for a service/services 
	Then  Participant should be able to access service requests directly
	When  Participant clicks on “My Choices” option
	Then  Participant should be able to view MyPlan, Support Enquiries, “Reviews and Rating” options 
	When  Participant clicks on “Reviews and Rating” option
	Then  Participant should be redirected to “My Reviews” page
	And   Should be able to view all provided reviews, provider name, Submitted date/time, Rated service name, Cancel Button
	When  Participant Clicks on Cancel button 
	Then  Participant should be redirected to My Plan Page
