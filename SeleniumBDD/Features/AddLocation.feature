﻿Feature: Add Location

@Add Location
Scenario: Add Location
	 
	Given As an Authorised person (NDIS registered provider or an unregistered provider) of an Organization launch “MyChooser” website
	When  Authorised person Register with MyChooser
	And   Logs in as provider administrator for the first time into provider account
	And   Creates Organization 
	And   Completes claim process with received Security Code 
	And   submits “Save” request with required details of the Organization on Add Brnading page
	Then  administrator should be suggested with “Add Location” button 
	When  administrator clicks on “Add Location” button 
	Then  administrator should be redirected to “Add a location” page
	When  administrator submits “Save” request with all the required Location, Contact details, Chosen Service area LGA’s, Facilities
	Then  administrator should be suggested with “Add another location”
	And   “Add services” button


@Claim - Add Location
Scenario: Add Organization Location (Claim)
	 
	Given As an Authorised person (NDIS registered provider or an unregistered provider) of an Organization launch “MyChooser” website
	When  Authorised person Register with MyChooser
	And   Logs in as provider administrator for the first time into provider account
	And   Claims Organisation Without SecurityCode
	And   completes “Claim Organisation” process with received security code
	And   submits “Save” request with required details of the Organization on Add Brnading page 
	And   provider clicks on “submit” button  with the Required Details, Uploads Logo, Uploads Banner Image
	Then  administrator should be suggested with “Add Location” button 
	When  administrator clicks on “Add Location” button 
	Then  administrator should be redirected to “Add a location” page
	When  administrator submits “Save” request with all the required Location, Contact details, Chosen Service area LGA’s, Facilities
	Then  administrator should be suggested with “Add another location”
	And   “Add services” button