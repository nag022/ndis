﻿Feature: Add Services
	

@Add Services
Scenario: Add Services
	Given As an Administrator Provider (NDIS registered or Unregistered provider) of an organization launch “MyChooser” website
	When  Authorised person Registers with MyChooser
	And   Logs in for the first time into provider accoount
	And   Creates  Organization
	And   Completes claim process with received Security Code, Adds Branding of the Organization 
	And   submits “Save” request with required details on Add Location page
	Then  provider should be suggested with “Add another Location”, “Add Services” button
	When  provider clicks on “Add services” button
	Then  provider should be redirected to “Support Category” page
	When  Provider submits “Continue” request with chosen support categories
	Then  provider should be redirected to “Registed Groups” page
	When  Provider submits “Continue” request with chosen registered groups
	Then  provider should be suggested with “Add Staff” button, “Do this later” link-text
	When  provider clicks on “Do This Later” link-text
	Then  provider should be redirected to provider(Organization) Profile Page


@Claim - Add Services
Scenario: Add Organization Services (Claim)
	Given As an Administrator Provider (NDIS registered or Unregistered provider) of an organization launch “MyChooser” website
	When  Authorised person Registers with MyChooser
	And   Logs in for the first time into provider accoount
	And   Claims an Organization Without SecurityCode
	And   Completes claim process with received Security Code, Adds Branding of the claimed Organization 
	And   submits “Save” request with required details on Add Location page
	Then  provider should be suggested with “Add another Location”, “Add Services” button
	When  provider clicks on “Add services” button
	Then  provider should be redirected to “Support Category” page
	When  Provider submits “Continue” request with chosen support categories
	Then  provider should be redirected to “Registed Groups” page
	When  Provider submits “Continue” request with chosen registered groups
	Then  provider should be suggested with “Add Staff” button, “Do this later” link-text
	When  provider clicks on “Do This Later” link-text
	Then  provider should be redirected to provider(Organization) Profile Page

 
	
