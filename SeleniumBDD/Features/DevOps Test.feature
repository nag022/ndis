﻿Feature: DevOps Test

@DevOps Test Runs
Scenario: No Results Found
	Given As an Anonymous User launch “My Chooser” website
	When  User clicks on Provider Portal 
	Then  User should be redirected to Provider Search Page
	When  User Search for a non-existing Organization in My Chooser 	
	Then  User should get an error message:“<seached Organization name> is not found. Click on the register button to add your organisation.”
	And   Should be suggested with “Register your organization” button

@DevOps Test Runs
Scenario: Search Organization Results
	Given As an Anonymous User or an authorised person of an organization launch “My Chooser” website
	When  User clicks on Provider Portal 
	Then  User should be redirected to Provider Search Page
	When  User search for an Existing Organization in My Chooser 
	Then  User should be able to view list of organizations results matching search criteria, Claim Option 

@DevOps Test Runs
Scenario: Provider Details
	Given As an Anonymous User launch “My Chooser” website
	When  User search for provider services with respective Suburb/Postcode
	Then  User should be redirected to Support category services page 
	When  User submits Continue request with chosen support categories 
	Then  User should be redirected to registered groups 
	And   Should be able to View Providers button for respective registered group
	When  User clicks on View Providers button 
	Then  User should be able to view list of Provider results providing the chosen service in selected suburb
	When  User clicks on respective View Provider button on provider tile 
	Then  User should be able to view Provider details

	
