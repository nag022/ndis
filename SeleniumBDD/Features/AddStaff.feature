﻿Feature: Add Staff

@CompleteOrganizationProfile
Scenario: Add Staff (Organization Profile With Staff)
	Given As an administrator provider of an organization(NDIS registered or Unregistered) launch “MyChooser” Website
	When  Provider Register with MyChooser as a User
	And   Logs in as provider administrator for the first time into provider organization account
	And   Creates an Organisation
	And   Completes claim process with received Security Code, Adds Branding, Adds Location
	And   submits “Continue” request with chosen Services
	Then  provider should be suggested with “Add Staff” Button, “Do this later” link-text
	When  provider clicks on “Add Staff” button
	Then  provider should be redirected to “Add Support Worker” page
	When  provider submits “Continue” request with required suport worker details, chosen support services 
	Then  provider should be redirected to new page suggesting provider with “Add Another Person” button, “Finish” option
	When  Provider Clicks on “Add Another Person” button 
	Then  provider should be redirected to “Add Support Worker” page
	When  provider submits Continue request with required suport worker details, support services 
	Then  provider should be redirected to new page suggesting provider with “Add Another Person” button, “Finish” option


Scenario: Profile Page (Organization Without Staff) 
	Given As an administrator provider of an organization(NDIS registered or Unregistered) launch “MyChooser” Website
    When  Provider Register with MyChooser as a User
	And   Logs in as provider administrator for the first time into provider organization account
	And   Creates an Organisation
	And   Completes claim process with received Security Code, Adds Branding, Adds Location
	And   submits “Continue” request with chosen Services 
	Then  provider should be suggested with “Add Staff” Button, “Do this later” link-text
	When  provider submits “Do This Later” link-text
	Then  provider should be redirected to provider Profile Page

@Claim - CompleteOrganizationProfile
@END to END
Scenario: Claim An Organization
	Given As an administrator provider of an organization(NDIS registered or Unregistered) launch “MyChooser” Website
    When  Provider Register with MyChooser as a User
	And   Logs in as provider administrator for the first time into provider organization account	
	And   Claims an Organisation Without SecurityCode
	And   Completes claim process with received Security Code, Adds Branding, Adds Location of claimed organisation
	And   submits “Continue” request with chosen Services
	Then  provider should be suggested with “Add Staff” Button, “Do this later” link-text
	When  provider clicks on “Add Staff” button
	Then  provider should be redirected to “Add Support Worker” page
	When  provider submits “Continue” request with required suport worker details, chosen support services 
	Then  provider should be redirected to new page suggesting provider with “Add Another Person” button, “Finish” option
	When  provider clicks on “Finish” option
	Then  provider should be redirected to claimed provider Profile Page	

@CompleteOrganizationProfile
@END to END
Scenario: Provider Organization Profile 
	Given As an administrator provider of an organization(NDIS registered or Unregistered) launch “MyChooser” Website
	When  Provider Register with MyChooser as a User
	And   Logs in as provider administrator for the first time into provider organization account
	And   Creates an Organisation
	And   Completes claim process with received Security Code, Adds Branding, Adds Location
	And   submits “Continue” request with chosen Services
	Then  provider should be suggested with “Add Staff” Button, “Do this later” link-text
	When  provider clicks on “Add Staff” button
	Then  provider should be redirected to “Add Support Worker” page
	When  provider submits “Continue” request with required suport worker details, chosen support services 
	Then  provider should be redirected to new page suggesting provider with “Add Another Person” button, “Finish” option
	When  provider clicks on “Finish” option
	Then  provider should be redirected to provider Profile Page	



	


