﻿Feature: Claim Organization

@mytag
Scenario: Claim Organization
	Given As an Authorised person (NDIS registered or unregistered) of an organization launch “MyChooser” website
	When  Provider Registers with MyChooser to Claim an Existing Organziation
	#And  Logs in as provider administrator for the first time 
	When   Authorised person clicks on provider button
	Then  Authorised person should be redirected to provider search page
	When  Authorised person search for an organization by Org name  
	Then  Authorised person should be able to view the list of Organizations matching the search criteria, Claim option  
	When  Authorised person clicks on respective organization Claim button
	Then  Authorised person should be redirected to claim page
	When  Authorised person Claims Organization Without SecurityCode
	And   Search agian for the organization to claim with the recieved security code
	Then  Authorised person should be redirected to claim page with pre-saved details
	When  Authorised person submits “Continue” request with Required Details, Security Code
	Then  Authorised person should be suggested with “Setup my profile” option