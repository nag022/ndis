﻿Feature: My Plan

@mytag
Scenario: My Plan (Signed-In - multiple LGA's)
Given As an Anonymous User launch “My Chooser” website
When  User sing-in to Participant account 
And   search for provider services with respective Suburb/Postcode
And   searched Suburb/Postcode is not in multiple LGA's	
Then  Participant should be redirected to Support categories page
And   Should be able to view support categories, searched suburb(read only), Continue button 	 	
When  Participant submits Continue request with chosen support categories 	
Then  Participant should be redirected to My Plan page	
And   Should be able to view Save plan, Edit Plan, View Provider buttons for respective registered groups
When  Participant Clicks on Save Plan Button
Then  Participant should get a successful message: “Successfully saved your plan <participant name>”
When  Participant clicks on “Edit Plan” button 
Then  Participant should be redirected to Support categories page
And   Should be able to edit chosen support categories, searched suburb
When  Participant submits continue request with edited support categories, suburb/postcode
Then  Participant should be redirected to My Plan page with edited/new support categories
And   Participant should be warned to save the edited plan
When  Participant clicks on “Yes” Link-Text to save edited/new support categories
Then  Participant should be displayed a successful message: “Successfully saved your plan <participant name>”
