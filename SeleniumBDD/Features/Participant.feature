﻿Feature: Participant

@mytag
Scenario: Provider Details	
Given As an Anonymous User launch “My Chooser” website
When  User search for provider services with respective Suburb/Postcode
Then  User should be redirected to Support category services page 
When  User submits Continue request with chosen support categories 
Then  User should be redirected to registered groups 
And   Should be able to View Providers button for respective registered group
When  User clicks on View Providers button 	
Then  User should be able to view list of Provider results providing the chosen service in selected suburb
When  User clicks on respective View Provider button on provider tile 	
Then  User should be able to view Provider details
When  User Clicks on Sign-in/Register Button
Then  User should be redirected to Signin Page
When  User Sigin to user account 
Then  User shoud be redirected to Enquire Request page
When  User submits service request with required Special requirements
Then  User should get confirmation message on screen with “Click here” link 
When  User clicks on “Click here” link 
Then  User should be redirected to service request page
And   should be able to view all the requested services with Date, Time, Status, Leave a review link-text and Cancel button
When  User clicks on “Cancel”" button 
Then  User Should be redirected to MyPlan page
When  User clicks on leave a review on respective service requested
Then  User should be redirected to Leave a review page
When  User provides “rating and review”
And   Clicks on Preview button
Then  User should be able to preview the provided “rating and review”
When  User clicks on “Send Rating” button
Then  User should be able to send review to the provider for the provided service

Scenario: Provider Details
Given As an Registered User launch “My Chooser” website
When  User sig-in to user account 
And   User search for provider services with respective Suburb/Postcode
Then  User should be redirected to Support category services page 
When  User submits Continue request with chosen support categories 
Then  User should be redirected to registered groups 
And   Should be able to View Providers button for respective registered group
When  User clicks on View Providers button 
Then  User should be able to view list of Provider results providing the chosen service in selected suburb
When  User clicks on respective View Provider button on provider tile 
Then  User should be able to view Provider details, “SUPPORT ENQUIRY(service request)” form
When  User submits service request with required Special requirements
Then  User should get confirmation message on screen with “Click here” link 
When  User clicks on “Click here” link 
Then  User should be redirected to service request page
And   should be able to view all the requested services with Date, Time, Status, Leave a review link-text and Cancel button
When  User clicks on “Cancel”" button 
Then  User Should be redirected to MyPlan page
When  User clicks on leave a review on respective service requested
Then  User should be redirected to Leave a review page
When  User provides “rating and review”
And   Clicks on Preview button
Then  User should be able to preview the provided “rating and review”
When  User clicks on “Send Rating” button
Then  User should be able to send review to the provider for the provided service







