﻿Feature: Service Request


@mytag
Scenario: Leave a review (signed in user)
	Given As a registered user launch “MyChooser” website
	When  User sign In to participant account	
	And   submits Find Services request with desired Suburb/Postcode
	Then  Participant should be redirected to Support Categories
	When  Participant slects support categories, Clicks on “View Provider” button of respective registered group
	And   clicks on respective “View Provider” button on provider tile 
	Then  Participant should be able to view Provider details, “SUPPORT ENQUIRY (Enquire Service)” form with pre-filled user details on enquire form
	When  Participant submits “Send Enquiry” request with required Special Requirements
	And   clicks on “Click here” link-text on cofirmation message
	Then  Participant should be redirected to Service Requests page displaying the status, provider name, Request date/time, Leave a Review link-text, Cancel Button
	When  Participant clicks on “Leave a Review” to provide “Review and Rating” of the recieved service
	Then  Participant should be redirected to “Reviews and Ratings” page
	When  Participant submits “Preview” request with provided “Review and Rating”
	Then  Participant should be redirected to preview page displaying the preview of review, Send Rating, Edit Rating buttons
	When  Participant submits Send Rating request 
	Then  Participant should be displayed a successful submit message:“Your review and rating for “Provide Name” has been saved. Click here to view all your reviews.”
	When  Participant clicks on “Click here” link-text on cofirmation message page
	Then  Participant should be redirected to My Reviews page displaying the provider name, Submited date/time, Rated service name, Cancel Button 


	
