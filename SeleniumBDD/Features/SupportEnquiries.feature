﻿Feature: Support Enquiries
	
@mytag
Scenario: Support Enquiries
	Given As a registered User launch “My Chooser” website
	When  User Sign-In to Participant account where Participant has already requested for a service 
	Then  Participant should be able to access service requests
	When  Participant submits “My Choices” option request
	Then  Participant should be able to view MyPlan, Support Enquries, “Review and Rating” options 
	When  Participant clicks on Support Enquries options
	Then  Participant should be redirected to Support Enquries page
	And   Should be able to view the  requested services, their status, provider name, Request date/time, Cancel Button
	When  Participant submits Cancel request 
	Then  Participant should be redirected to MyPlan Page
