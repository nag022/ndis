﻿Feature: Provider Search / Create Organization	

@Signed In user
Scenario: Register Organization 
	Given As a provider (NDIS registered provider or an unregistered provider) launch “MyChooser” website
	When  Provider Registers with MyChooser 
	And   Logs in as provider administrator for the first time to create Organization
	When  provider clicks on Provider button
	Then  Provider should be redirected to provider search page
	When  provider Search for an organization by name
	And   the searched organization is not listed in the NDIS database
	Then  provider should be suggested with Register a new organization button
	When  provider clicks on Register a new organization button 
	Then  provider Should be able to view Organization Registration Page
	When  provider Clicks on “Continue” button with all the Required Details
	Then  provider should be redirected to a new page with successful registration 
	When  provider Search with organization name to claim
	Then  provider should be able to view the list of Organizations matching the search criteria, Claim option  
	When  provider clicks on respective organization Claim button
	Then  provider should be redirected to claim page
	When  provider submits “Continue” request with Required Details, Security Code
	Then  provider should be suggested with “Setup my profile” option

	


	


