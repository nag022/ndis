﻿Feature: Add Branding

@Add Branding 
Scenario: Add Branding 
	Given As a provider administrator (NDIS registered provider or an unregistered provider) launch “MyChooser” website
	When  Provider Register with MyChooser 
	When  provider Logs in as provider administrator for the first time into provider account
	And   Creates an Organization
	And   completes “Claim this Organization” process with received security code
	Then  provider should be suggested with “Setup Organization profile” option 
	When  provider clicks on “Setup my profile” button 
	Then  provider should be redirected to “Add branding” page
	When  provider enters all the Required Details, Uploads Logo, Banner Image 
	And   clicks on “submit” button
	Then  provider should be able to Add Organization Branding successfully
	And   Should be suggested with AddLocation Button

@Claim - Add Branding (Existing Organization in the Database)
Scenario: Add Organization Branding (Claim)
	Given As a provider administrator (NDIS registered provider or an unregistered provider) launch “MyChooser” website
	When  Provider Register with MyChooser 
	When  provider Logs in as provider administrator for the first time into provider account
	And   Claims Organization Without SecurityCode
	And   completes “Claim Organization” process with received security code
	Then  provider should be suggested with “Setup Organization profile” option 
	When  provider clicks on “Setup my profile” button 
	Then  provider should be redirected to “Add branding” page
	When  provider enters all the Required Details, Uploads Logo, Uploads Banner Image 
	And   clicks on “submit” button 
	Then  provider should be able to Add Organization Branding successfully
	And   Should be suggested with AddLocation Button