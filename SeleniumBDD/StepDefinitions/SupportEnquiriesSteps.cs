﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class SupportEnquiriesSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public ServiceRequestPage _serviceRequestPage;
        public DevOpsTestPage _devOpsTestPage;
        public ReviewsandRatingsPage _reviewsandratingsPage;
        public ProviderSearchPage _providerSearchPage;
        public SupportEnquiriesPage _supportEnquiriesPage;

        public SupportEnquiriesSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _serviceRequestPage = new ServiceRequestPage(WebDriver);
            _reviewsandratingsPage = new ReviewsandRatingsPage(WebDriver);
            _devOpsTestPage = new DevOpsTestPage(WebDriver);
            _providerSearchPage = new ProviderSearchPage(WebDriver);
            _supportEnquiriesPage = new SupportEnquiriesPage(WebDriver);

        }

        [Given(@"As a registered User launch “My Chooser” website")]
        public void GivenAsARegisteredUserLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }
        
        [When(@"User Sign-In to Participant account where Participant has already requested for a service")]
        public void WhenUserSign_InToParticipantAccountWhereParticipantHasAlreadyRequestedForAService()
        {
            _loginPage.CallSignPage();
            _loginPage.LoginToApplication();
            //_providerSearchPage.LoginApplication();
            //_providerSearchPage.SelectProvider();
            _providerSearchPage.SelectProviderSearch();
        }

        [Then(@"Participant should be able to access service requests")]
        public void ThenParticipantShouldBeAbleToAccessServiceRequests()
        {
            _reviewsandratingsPage.MyChoicesOption();
        }

        [When(@"Participant submits “My Choices” option request")]
        public void WhenParticipantSubmitsMyChoicesOptionRequest()
        {
            _reviewsandratingsPage.MyChoices();
        }

        [Then(@"Participant should be able to view MyPlan, Support Enquries, “Review and Rating” options")]
        public void ThenParticipantShouldBeAbleToViewMyPlanSupportEnquriesReviewAndRatingOptions()
        {
            _supportEnquiriesPage.SupportBtn();
        }

        [When(@"Participant clicks on Support Enquries options")]
        public void WhenParticipantClicksOnSupportEnquriesOptions()
        {
            _supportEnquiriesPage.SupportEnquiries();
            _supportEnquiriesPage.Include();
        }

        [Then(@"Participant should be redirected to Support Enquries page")]
        public void ThenParticipantShouldBeRedirectedToSupportEnquriesPage()
        {
            _reviewsandratingsPage.Cancel();
           
        }

        [Then(@"Should be able to view the  requested services, their status, provider name, Request date/time, Cancel Button")]
        public void ThenShouldBeAbleToViewTheRequestedServicesTheirStatusProviderNameRequestDateTimeCancelButton()
        {
            _reviewsandratingsPage.Cancel();
        }

        [When(@"Participant submits Cancel request")]
        public void WhenParticipantSubmitsCancelRequest()
        {
            _reviewsandratingsPage.MyPlanPage();
        }

        [Then(@"Participant should be redirected to MyPlan Page")]
        public void ThenParticipantShouldBeRedirectedToMyPlanPage()
        {
            _devOpsTestPage.ViewProviderOption();
        }
    }
}
