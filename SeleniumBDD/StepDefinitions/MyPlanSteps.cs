﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class MyPlanSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public ServiceRequestPage _serviceRequestPage;
        public MyPlanPage _myplanPage;
        public ProviderSearchPage _providerSearchPage;
        DevOpsTestPage _devOpsTestPage;

        public MyPlanSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _serviceRequestPage = new ServiceRequestPage(WebDriver);
            _myplanPage = new MyPlanPage(WebDriver);
            _providerSearchPage = new ProviderSearchPage(WebDriver);
            _devOpsTestPage = new DevOpsTestPage(WebDriver);
        }


        [When(@"User sing-in to Participant account")]
        public void WhenUserSing_InToParticipantAccount()
        {
            _loginPage.CallSignPage();
            _loginPage.LoginToApplication();
            //_providerSearchPage.LoginApplication();
            //_providerSearchPage.SelectProvider();
            _providerSearchPage.SelectProviderSearch();
        }


        [When(@"search for provider services with respective Suburb/Postcode")]
        public void WhenSearchForProviderServicesWithRespectiveSuburbPostcode()
        {
            _devOpsTestPage.SearchSuburb();
        }

        [When(@"searched Suburb/Postcode is not in multiple LGA's")]
        public void WhenSearchedSuburbPostcodeIsNotInMultipleLGAS()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"Participant should be redirected to Support categories page")]
        public void ThenParticipantShouldBeRedirectedToSupportCategoriesPage()
        {
            _devOpsTestPage.SupportCategoryPage();
        }

        [Then(@"Should be able to view support categories, searched suburb\(read only\), Continue button")]
        public void ThenShouldBeAbleToViewSupportCategoriesSearchedSuburbReadOnlyContinueButton()
        {
            _devOpsTestPage.SupportCategoryPage();
        }

        [When(@"Participant submits Continue request with chosen support categories")]
        public void WhenParticipantSubmitsContinueRequestWithChosenSupportCategories()
        {
            _devOpsTestPage.SelectSupportCategoy();
        }

        [Then(@"Participant should be redirected to My Plan page")]
        public void ThenParticipantShouldBeRedirectedToMyPlanPage()
        {
            _devOpsTestPage.ViewProviderOption();
        }

        [Then(@"Should be able to view Save plan, Edit Plan, View Provider buttons for respective registered groups")]
        public void ThenShouldBeAbleToViewSavePlanEditPlanViewProviderButtonsForRespectiveRegisteredGroups()
        {
            _devOpsTestPage.ViewProviderOption();
        }

        [When(@"Participant Clicks on Save Plan Button")]
        public void WhenParticipantClicksOnSavePlanButton()
        {
            _myplanPage.SaveMyPlan();
        }

        [Then(@"Participant should get a successful message: “Successfully saved your plan (.*)”")]
        public void ThenParticipantShouldGetASuccessfulMessageSuccessfullySavedYourPlan(string p0)
        {
            _myplanPage.SuccessfulMessage();
        }

        [When(@"Participant clicks on “Edit Plan” button")]
        public void WhenParticipantClicksOnEditPlanButton()
        {
            _myplanPage.EditMyPlan();
        }

        [Then(@"Should be able to edit chosen support categories, searched suburb")]
        public void ThenShouldBeAbleToEditChosenSupportCategoriesSearchedSuburb()
        {
            _devOpsTestPage.SupportCategoryPage();
        }


        [When(@"Participant submits continue request with edited support categories, suburb/postcode")]
        public void WhenParticipantSubmitsContinueRequestWithEditedSupportCategoriesSuburbPostcode()
        {
            _myplanPage.SelectSupportCategoy();
        }

        [Then(@"Participant should be redirected to My Plan page with edited/new support categories")]
        public void ThenParticipantShouldBeRedirectedToMyPlanPageWithEditedNewSupportCategories()
        {
            _devOpsTestPage.ViewProviderOption();
        }

        [Then(@"Participant should be warned to save the edited plan")]
        public void ThenParticipantShouldBeWarnedToSaveTheEditedPlan()
        {
            _myplanPage.Warning();
        }

        [When(@"Participant clicks on “Yes” Link-Text to save edited/new support categories")]
        public void WhenParticipantClicksOnYesLink_TextToSaveEditedNewSupportCategories()
        {
            _myplanPage.SelectYES();
        }
         
        [Then(@"Participant should be displayed a successful message: “Successfully saved your plan (.*)”")]
        public void ThenParticipantShouldBeDisplayedASuccessfulMessageSuccessfullySavedYourPlan(string p0)
        {
            _myplanPage.SuccessfulMessage();
        }
    }
}
