﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class AddLocationSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public UserRegistrationPage _userRegistrationPage;
        public GmailPage _gmailPage;
        public ProviderSearchPage _providerPage;
        public ClaimOrganizationPage _claimOrganizationPage;
        public AddBrandingPage _addbrandingPage;
        public AddClaimBrandingPage _addClaimBrandingPage;
        public AddLocationPage _addlocationPage;

        public AddLocationSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _claimOrganizationPage = new ClaimOrganizationPage(WebDriver);
            _addClaimBrandingPage = new AddClaimBrandingPage(WebDriver);
            _addbrandingPage = new AddBrandingPage(WebDriver);
            _addlocationPage = new AddLocationPage(WebDriver);
        }

        [Given(@"As an Authorised person \(NDIS registered provider or an unregistered provider\) of an Organization launch “MyChooser” website")]
        public void GivenAsAnAuthorisedPersonNDISRegisteredProviderOrAnUnregisteredProviderOfAnOrganizationLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"Authorised person Register with MyChooser")]
        public void WhenAuthorisedPersonRegisterWithMyChooser()
        {
            _userRegistrationPage.CallRegisterButtn();
            _userRegistrationPage.FillRegistrationForm();
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            //_gmailPage.GmailLogin();

            _gmailPage.OpenRegistrationEmail();
            _gmailPage.CompleteRegistrationMyChooser();
            _userRegistrationPage.ChoosePassword();
        }

        [When(@"Logs in as provider administrator for the first time into provider account")]
        public void WhenAuthorisedPersonLogsInAsProviderAdministratorForTheFirstTimeIntoProviderAccount()
        {
            //_loginPage.CallSignPage();
            _userRegistrationPage.SigninToApplication();

        }

        [When(@"Creates Organization")]
        public void WhenCreatesOrganization()
        {
            _providerPage.SelectProvider();
            _providerPage.SearchOrganizationByName();
            _providerPage.RegisterNewOrganizationOption();
            _providerPage.SelectRegisterOrganization();
            _providerPage.EnterRegistrationDetails();
            _providerPage.SubmitRegistrationForm();
        }

        [When(@"Completes claim process with received Security Code")]
        public async Task WhenCompletesClaimProcessWithReceivedSecurityCode()
        {
            //_providerPage.SelectProviderSearch();
            _providerPage.SearchOrgPage();
            _providerPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _providerPage.GetSecurityCodeAsync();
            _providerPage.ClaimTheOrganizationWithSecurityCode();

        }

        [When(@"submits “Save” request with required details of the Organization on Add Brnading page")]
        public void WhenSubmitsSaveRequestWithRequiredDetailsOfTheOrganizationOnAddBrnadingPage()
        {
            _addbrandingPage.SelectSetUpMyProfileOption();
            _addbrandingPage.AddOrganizationDetails();
            _addbrandingPage.SubmitOrganizationDetails();
        }


        [Then(@"administrator should be suggested with “Add Location” button")]
        public void ThenAdministratorShouldBeSuggestedWithAddLocationButton()
        {
            //Assert.IsTrue(_addbrandingPage.AddLocationOption());
            if (_addbrandingPage.AddLocationOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider can view AddLocation Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider can not view AddLocation Option", null);
            }
        }

        [When(@"administrator clicks on “Add Location” button")]
        public void WhenAdministratorClicksOnAddLocationsButton()
        {
            _addlocationPage.SelectAddLocationButton();
        }

        [Then(@"administrator should be redirected to “Add a location” page")]
        public void ThenAdministratorShouldBeRedirectedToAddALocationPage()
        {
            Assert.IsTrue(_addlocationPage.AddLocationName());
        }

        [When(@"administrator submits “Save” request with all the required Location, Contact details, Chosen Service area LGA’s, Facilities")]
        public void WhenAdministratorSubmitsSaveRequestWithAllTheRequiredLocationContactDetailsChosenServiceAreaLGASFacilities()
        {
            _addlocationPage.EnterLocContactDetails();
            _addlocationPage.SelectSave();

        }

        [Then(@"administrator should be suggested with “Add another location”")]
        public void ThenAdministratorShouldBeSuggestedWithAddAnotherLocation()
        {
            Assert.IsTrue(_addlocationPage.AddAnotherLocationButton());
            if (_addlocationPage.AddAnotherLocationButton())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider suggested with “Add another” location", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not suggested with “Add another” location", null);
            }
        }

        [Then(@"“Add services” button")]
        public void ThenAddServicesButton()
        {
            //Assert.IsTrue(_addlocationPage.AddServicesButton());
            if (_addlocationPage.AddServicesButton())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider suggested with “AddServices” Button", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not suggested with “AddServices” Button", null);
            }
        }

        [When(@"Claims Organisation Without SecurityCode")]
        public void WhenClaimsOrganisationWithoutSecurityCode()
        {
            _providerPage.SelectProvider();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            _claimOrganizationPage.ClaimTheOrganization();
        }

        [When(@"completes “Claim Organisation” process with received security code")]
        public async Task WhenCompletesClaimOrganisationProcessWithReceivedSecurityCode()
        {
            _providerPage.SelectProviderSearch();
            _providerPage.SearchOrgPage();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _claimOrganizationPage.GetSecurityCodeAsync();
            _claimOrganizationPage.ClaimTheOrganizationWithSecurityCode();
        }

        [When(@"provider clicks on “submit” button  with the Required Details, Uploads Logo, Uploads Banner Image")]
        public void WhenProviderClicksOnSubmitButtonWithTheRequiredDetailsUploadsLogoUploadsBannerImage()
        {
            _addClaimBrandingPage.AddClaimOrganizationDetails();
            _addClaimBrandingPage.SubmitClaimOrganizationDetails();
        }


    }
}
