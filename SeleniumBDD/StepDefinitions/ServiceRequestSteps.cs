﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class ServiceRequestSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public ServiceRequestPage _serviceRequestPage;
        public DevOpsTestPage _devOpsTestPage;
        public ProviderSearchPage _providerSearchPage;
        public ReviewsandRatingsPage _reviewsandratingsPage;

        public ServiceRequestSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _serviceRequestPage = new ServiceRequestPage(WebDriver);
            _devOpsTestPage = new DevOpsTestPage(WebDriver);
            _providerSearchPage = new ProviderSearchPage(WebDriver);
            _reviewsandratingsPage = new ReviewsandRatingsPage(WebDriver);
        }



        [Given(@"As a registered user launch “MyChooser” website")]
        public void GivenAsARegisteredUserLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }
        
        [When(@"User sign In to participant account")]
        public void WhenUserSignInToParticipantAccount()
        {
            _loginPage.CallSignPage();
            _loginPage.LoginToApplication();
            //_providerSearchPage.LoginApplication();
            //_providerSearchPage.SelectProvider();
            _providerSearchPage.SelectProviderSearch();
            //_serviceRequestPage.Mychoices();
            
        }
        
        [When(@"submits Find Services request with desired Suburb/Postcode")]
        public void WhenSubmitsFindServicesRequestWithDesiredSuburbPostcode()
        {
            _devOpsTestPage.SearchSuburb();
        }

        [Then(@"Participant should be redirected to Support Categories")]
        public void ThenParticipantShouldBeRedirectedToSupportCategories()
        {
            _devOpsTestPage.SupportCategoryPage();
        }

        [When(@"Participant slects support categories, Clicks on “View Provider” button of respective registered group")]
        public void WhenParticipantSlectsSupportCategoriesClicksOnViewProviderButtonOfRespectiveRegisteredGroup()
        {
            _devOpsTestPage.SelectSupportCategoy();
            _devOpsTestPage.SelectViewProviders();
        }
        
        [When(@"clicks on respective “View Provider” button on provider tile")]
        public void WhenClicksOnRespectiveViewProviderButtonOnProviderTile()
        {
            _devOpsTestPage.SelectProvider();
        }

        [Then(@"Participant should be able to view Provider details, “SUPPORT ENQUIRY \(Enquire Service\)” form with pre-filled user details on enquire form")]
        public void ThenParticipantShouldBeAbleToViewProviderDetailsSUPPORTENQUIRYEnquireServiceFormWithPre_FilledUserDetailsOnEnquireForm()
        {
            _devOpsTestPage.ConfirmProviderPage();
        }

        [When(@"Participant submits “Send Enquiry” request with required Special Requirements")]
        public void WhenParticipantSubmitsSendEnquiryRequestWithRequiredSpecialRequirements()
        {
            _serviceRequestPage.SendEnquiryButton();
        }
        
        [When(@"clicks on “Click here” link-text on cofirmation message")]
        public void WhenClicksOnClickHereLink_TextOnCofirmationMessage()
        {
            _serviceRequestPage.ClickHereLink();
        }

        [Then(@"Participant should be redirected to Service Requests page displaying the status, provider name, Request date/time, Leave a Review link-text, Cancel Button")]
        public void ThenParticipantShouldBeRedirectedToServiceRequestsPageDisplayingTheStatusProviderNameRequestDateTimeLeaveAReviewLink_TextCancelButton()
        {
            _reviewsandratingsPage.Cancel();
        }

        [When(@"Participant clicks on “Leave a Review” to provide “Review and Rating” of the recieved service")]
        public void WhenParticipantClicksOnLeaveAReviewToProvideReviewAndRatingOfTheRecievedService()
        {
            _serviceRequestPage.LeaveaReview();
        }

        [Then(@"Participant should be redirected to “Reviews and Ratings” page")]
        public void ThenParticipantShouldBeRedirectedToReviewsAndRatingsPage()
        {
            _serviceRequestPage.GiveComment();
        }

        [When(@"Participant submits “Preview” request with provided “Review and Rating”")]
        public void WhenParticipantSubmitsPreviewRequestWithProvidedReviewAndRating()
        {
            _serviceRequestPage.RateRating();
            _serviceRequestPage.GiveComments();
            _serviceRequestPage.RateRating();
            _serviceRequestPage.RateRating();
            _serviceRequestPage.RateRating();
            _serviceRequestPage.RateRating();


        }

        [Then(@"Participant should be redirected to preview page displaying the preview of review, Send Rating, Edit Rating buttons")]
        public void ThenParticipantShouldBeRedirectedToPreviewPageDisplayingThePreviewOfReviewSendRatingEditRatingButtons()
        {
            _serviceRequestPage.BtnSendRatings();
        }

        [When(@"Participant submits Send Rating request")]
        public void WhenParticipantSubmitsSendRatingRequest()
        {
            _serviceRequestPage.SendRating();
        }

        [Then(@"Participant should be displayed a successful submit message:“Your review and rating for “Provide Name” has been saved\. Click here to view all your reviews\.”")]
        public void ThenParticipantShouldBeDisplayedASuccessfulSubmitMessageYourReviewAndRatingForProvideNameHasBeenSaved_ClickHereToViewAllYourReviews_()
        {
            _serviceRequestPage.ClicksHereLinkText();
        }

        [When(@"Participant clicks on “Click here” link-text on cofirmation message page")]
        public void WhenParticipantClicksOnClickHereLink_TextOnCofirmationMessagePage()
        {
            _serviceRequestPage.ClicksHereLink();
        }
            
        [Then(@"Participant should be redirected to My Reviews page displaying the provider name, Submited date/time, Rated service name, Cancel Button")]
        public void ThenParticipantShouldBeRedirectedToMyReviewsPageDisplayingTheProviderNameSubmitedDateTimeRatedServiceNameCancelButton()
        {
            _serviceRequestPage.ConfirmMyReviews();
        }
    }
}
