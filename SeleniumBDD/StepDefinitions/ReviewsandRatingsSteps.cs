﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class ReviewsandRatingsSteps
    {
        public static IWebDriver WebDriver;
        public ReviewsandRatingsPage _reviewsandratingsPage;
        public LoginPage _loginPage;
        public DevOpsTestPage _devOpsTestPage;
        public ProviderSearchPage _providerSearchPage;
        public ServiceRequestPage _serviceRequestPage;

        public ReviewsandRatingsSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _reviewsandratingsPage = new ReviewsandRatingsPage(WebDriver);
            _devOpsTestPage = new DevOpsTestPage(WebDriver);
            _providerSearchPage = new ProviderSearchPage(WebDriver);
            _serviceRequestPage = new ServiceRequestPage(WebDriver);
     
        }

        [Given(@"As a registered user launch “My Chooser” website")]
        public void GivenAsARegisteredUserLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"User Sign-In to Participant account where Participant has already requested for a service/services")]
        public void WhenUserSign_InToParticipantAccountWhereParticipantHasAlreadyRequestedForAServiceServices()
        {
            _loginPage.CallSignPage();
            _loginPage.LoginToApplication();
            //_providerSearchPage.LoginApplication();
            //_providerSearchPage.SelectProvider();
            _providerSearchPage.SelectProviderSearch();
        }

        [Then(@"Participant should be able to access service requests directly")]
        public void ThenParticipantShouldBeAbleToAccessServiceRequestsDirectly()
        {
            _reviewsandratingsPage.MyChoicesOption();
        }

        [When(@"Participant clicks on “My Choices” option")]
        public void WhenParticipantClicksOnMyChoicesOption()
        {
            _reviewsandratingsPage.MyChoices();
        }

        [Then(@"Participant should be able to view MyPlan, Support Enquiries, “Reviews and Rating” options")]
        public void ThenParticipantShouldBeAbleToViewMyPlanSupportEnquiriesReviewsAndRatingOptions()
        {
            _reviewsandratingsPage.RandROption();
            
        }

        [When(@"Participant clicks on “Reviews and Rating” option")]
        public void WhenParticipantClicksOnReviewsAndRatingOption()
        {
            _reviewsandratingsPage.ReviewsRatings();
        }

        [Then(@"Participant should be redirected to “My Reviews” page")]
        public void ThenParticipantShouldBeRedirectedToMyReviewsPage()
        {
            _reviewsandratingsPage.Cancel();
        }

        [Then(@"Should be able to view all provided reviews, provider name, Submitted date/time, Rated service name, Cancel Button")]
        public void ThenShouldBeAbleToViewAllProvidedReviewsProviderNameSubmittedDateTimeRatedServiceNameCancelButton()
        {
            _reviewsandratingsPage.Cancel();
        }

        [When(@"Participant Clicks on Cancel button")]
        public void WhenParticipantClicksOnCancelButton()
        {
            _reviewsandratingsPage.MyPlanPage();
        }               
        
        [Then(@"Participant should be redirected to My Plan Page")]
        public void ThenParticipantShouldBeRedirectedToMyPlanPage()
        {
            _devOpsTestPage.ViewProviderOption();
        }
    }
}
