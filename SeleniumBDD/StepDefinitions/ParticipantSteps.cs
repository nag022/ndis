﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.StepDefinitions;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class Participant
         
    {
        public IWebDriver WebDriver;
        public LoginPage _loginPage;
        public DevOpsTestPage _devopsTestPage;
        public ParticipantPage _participantPage;

        public Participant()
        {
            WebDriver = TestSetup.WebDriver;
            _participantPage = new ParticipantPage(WebDriver);
            _loginPage = new LoginPage(WebDriver);
            _devopsTestPage = new DevOpsTestPage(WebDriver);
        }


        [Given(@"As an Anonymous User launch “My Chooser” website")]
        public void GivenAsAnAnonymousUserLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"User search for provider services with respective Suburb/Postcode")]
        public void WhenUserSearchForProviderServicesWithRespectiveSuburbPostcode()
        {
            _devopsTestPage.SearchSuburb();
        }

        [Then(@"User should be redirected to Support category services page")]
        public void ThenUserShouldBeRedirectedToSupportCategoryServicesPage()
        {
            _devopsTestPage.SupportCategoryPage();
        }

        [When(@"User submits Continue request with chosen support categories")]
        public void WhenUserSubmitsContinueRequestWithChosenSupportCategories()
        {
            _devopsTestPage.SelectSupportCategoy();
        }

        [Then(@"User should be redirected to registered groups")]
        public void ThenUserShouldBeRedirectedToRegisteredGroups()
        {
            _devopsTestPage.ViewProviderOption();
        }

        [Then(@"Should be able to View Providers button for respective registered group")]
        public void ThenShouldBeAbleToViewProvidersButtonForRespectiveRegisteredGroup()
        {
            _devopsTestPage.ViewProviderOption();
        }

        [When(@"User clicks on View Providers button")]
        public void WhenUserClicksOnViewProvidersButton()
        {
            _devopsTestPage.SelectViewProviders();
            Thread.Sleep(5000);
        }

        [Then(@"User should be able to view list of Provider results providing the chosen service in selected suburb")]
        public void ThenUserShouldBeAbleToViewListOfProviderResultsProvidingTheChosenServiceInSelectedSuburb()
        {
            _devopsTestPage.Checkbox();
            if (_devopsTestPage.Checkbox())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User is able to view Providers List", null);
            }

            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to view Providers List", null);
            }
        }

        [When(@"User clicks on respective View Provider button on provider tile")]
        public void WhenUserClicksOnRespectiveViewProviderButtonOnProviderTile()
        {
            _devopsTestPage.SelectProvider();
        }

        [Then(@"User should be able to view Provider details")]
        public void ThenUserShouldBeAbleToViewProviderDetails()
        {
            //_devopsTestPage.ConfirmProviderPage();
            if (_devopsTestPage.ConfirmProviderPage())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User is able to view Provider Details", null);
            }

            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to view Provider Details", null);
            }
        }

        [When(@"User Clicks on Sign-in/Register Button")]
        public void WhenUserClicksOnSign_InRegisterButton()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User should be redirected to Signin Page")]
        public void ThenUserShouldBeRedirectedToSigninPage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User Sigin to user account")]
        public void WhenUserSiginToUserAccount()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User shoud be redirected to Enquire Request page")]
        public void ThenUserShoudBeRedirectedToEnquireRequestPage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User submits service request with required Special requirements")]
        public void WhenUserSubmitsServiceRequestWithRequiredSpecialRequirements()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User should get confirmation message on screen with “Click here” link")]
        public void ThenUserShouldGetConfirmationMessageOnScreenWithClickHereLink()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on “Click here” link")]
        public void WhenUserClicksOnClickHereLink()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User should be redirected to service request page")]
        public void ThenUserShouldBeRedirectedToServiceRequestPage()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"should be able to view all the requested services with Date, Time, Status, Leave a review link-text and Cancel button")]
        public void ThenShouldBeAbleToViewAllTheRequestedServicesWithDateTimeStatusLeaveAReviewLink_TextAndCancelButton()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on “Cancel”"" button")]
        public void WhenUserClicksOnCancelButton()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User Should be redirected to MyPlan page")]
        public void ThenUserShouldBeRedirectedToMyPlanPage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on leave a review on respective service requested")]
        public void WhenUserClicksOnLeaveAReviewOnRespectiveServiceRequested()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User should be redirected to Leave a review page")]
        public void ThenUserShouldBeRedirectedToLeaveAReviewPage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User provides “rating and review”")]
        public void WhenUserProvidesRatingAndReview()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"Clicks on Preview button")]
        public void WhenClicksOnPreviewButton()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User should be able to preview the provided “rating and review”")]
        public void ThenUserShouldBeAbleToPreviewTheProvidedRatingAndReview()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on “Send Rating” button")]
        public void WhenUserClicksOnSendRatingButton()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User should be able to send review to the provider for the provided service")]
        public void ThenUserShouldBeAbleToSendReviewToTheProviderForTheProvidedService()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"As an Registered User launch “My Chooser” website")]
        public void GivenAsAnRegisteredUserLaunchMyChooserWebsite()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User sig-in to user account")]
        public void WhenUserSig_InToUserAccount()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User should be able to view Provider details, “SUPPORT ENQUIRY\(service request\)” form")]
        public void ThenUserShouldBeAbleToViewProviderDetailsSUPPORTENQUIRYServiceRequestForm()
        {
            ScenarioContext.Current.Pending();
        }

    }
}
