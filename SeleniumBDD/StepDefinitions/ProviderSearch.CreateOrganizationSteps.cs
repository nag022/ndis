﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Linq;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class ProviderSearchSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public UserRegistrationPage _userRegistrationPage;
        public GmailPage _gmailPage;
        public ProviderSearchPage _providerPage;
        public ClaimOrganizationPage _claimOrganizationPage;

        public ProviderSearchSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _claimOrganizationPage = new ClaimOrganizationPage(WebDriver);
        }

        [Given(@"As a provider \(NDIS registered provider or an unregistered provider\) launch “MyChooser” website")]
        public void GivenAsAProviderNDISRegisteredProviderOrAnUnregisteredProviderLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"Provider Registers with MyChooser")]
        public void WhenProviderRegistersWithMyChooser()
        {
            _userRegistrationPage.CallRegisterButtn();
            _userRegistrationPage.FillRegistrationForm();
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            //_gmailPage.GmailLogin();

            _gmailPage.OpenRegistrationEmail();
            _gmailPage.CompleteRegistrationMyChooser();
            _userRegistrationPage.ChoosePassword();
            
        }

        [When(@"Logs in as provider administrator for the first time to create Organization")]
        public void WhenLogsInAsProviderAdministratorForTheFirstTimeToCreateOrganization()
        {
            //_loginPage.CallSignPage();
            _userRegistrationPage.SigninToApplication();
        }

        [When(@"provider clicks on Provider button")]
        public void ProviderclicksonProviderbutton()
        {
            _providerPage.SelectProvider();
           
        }

        [Then(@"Provider should be redirected to provider search page")]
        public void Providershouldberedirectedtoprovidersearchpage()
        {
            Assert.IsTrue(_providerPage.SearchOrgPage());
        }

        [When(@"provider Search for an organization by name")]
        public void WhenProviderSearchForAnOrganizationByName()
        {
            _providerPage.SearchOrganizationByName();
        }

        [When(@"the searched organization is not listed in the NDIS database")]
        public void WhenTheSearchedOrganizationIsNotListedInTheNDISDatabase()
        {
            _providerPage.OrganizationNotAvailableInDataBase();
        }

        [Then(@"provider should be suggested with Register a new organization button")]
        public void ThenProviderShouldBeSuggestedWithRegisterANewOrganizationButton()
        {
            //Assert.IsTrue(_providerPage.RegisterNewOrganizationOption());
            if (_providerPage.RegisterNewOrganizationOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can see the Register NewOrganization Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not see the Register NewOrganization Option ", null);
            }
        }

        [When(@"provider clicks on Register a new organization button")]
        public void WhenProviderClicksOnRegisterANewOrganizationButton()
        {
            _providerPage.SelectRegisterOrganization();
        }

        [Then(@"provider Should be able to view Organization Registration Page")]
        public void ThenProviderShouldBeAbleToViewOrganizationRegistrationPage()
        {
            //Assert.IsTrue(_providerPage.ConfirmRegistrationPage());
            if (_providerPage.ConfirmRegistrationPage())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can see the ConfirmRegistrationPage", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not see the ConfirmRegistrationPage ", null);
            }
        }


        [When(@"provider Clicks on “Continue” button with all the Required Details")]
        public void WhenProviderClicksOnContinueButtonWithAllTheRequiredDetails()
        {
            _providerPage.EnterRegistrationDetails();
            _providerPage.SubmitRegistrationForm();
        }

        [Then(@"provider should be redirected to a new page with successful registration")]
        public void ThenProviderShouldBeRedirectedToANewPageWithSuccessfulRegistration()
        {
            if (WebDriver.FindElements(By.XPath("//h1[contains(@class,'col-md-12')]")).ElementAt(0).GetAttribute("innerText").Contains("Thank you for registering with MyChooser"))
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Registration Confirmed", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User Registration Failed ", null);
            }

            Assert.IsTrue(_providerPage.ConfirmClaim());
        }

        //[Then(@"provider should be registered successfully")]
        //public void ThenProviderShouldBeRegisteredSuccessfully()
        //{
        //    Assert.IsTrue(_providerPage.ConfirmRegistration());
        //}

        [When(@"provider Search with organization name to claim")]
        public void WhenProviderSearchWithOrganizationNameToClaim()
        {
            _providerPage.SelectProviderSearch();
            _providerPage.SearchOrganizationByName();
        }

        [Then(@"provider should be able to view the list of Organizations matching the search criteria, Claim option")]
        public void ThenProviderShouldBeAbleToViewTheListOfOrganizationsMatchingTheSearchCriteriaClaimOption()
        {
            _claimOrganizationPage.ClaimOption();
        }

        [When(@"provider clicks on respective organization Claim button")]
        public void WhenProviderClicksOnRespectiveOrganizationClaimButton()
        {
            _providerPage.ClaimButton();
        }

        [Then(@"provider should be redirected to claim page")]
        public void ThenProviderShouldBeRedirectedToClaimPage()
        {
            //Assert.IsTrue(_providerPage.ClaimOrganization());
            if (_providerPage.ClaimOrganization())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Can view ClaimOrganization Page", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User Can not view ClaimOrganization Page", null);
            }
        }

        [When(@"provider submits “Continue” request with Required Details, Security Code")]
        public async Task WhenProviderSubmitsContinueRequestWithRequiredDetailsSecurityCode()
        {
            String sCode = await _providerPage.GetSecurityCodeAsync();
            _providerPage.ClaimTheOrganizationWithSecurityCode();
        }

        [Then(@"provider should be suggested with “Setup my profile” option")]
        public void ThenProviderShouldBeSuggestedWithSetupMyProfileOption()
        {
            //Assert.IsTrue(_providerPage.SetUpMyProfileOption());
            if (_providerPage.SetUpMyProfileOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User Can view SetUpMyProfile Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User Can not view SetUpMyProfile Option", null);
            }
        }

        
    }
}


