﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class AddStaffSteps
    {
        public IWebDriver WebDriver;
        public LoginPage _loginPage;
        public UserRegistrationPage _userRegistrationPage;
        public GmailPage _gmailPage;
        //public YahooPage _gmailPage;
        public ProviderSearchPage _providerPage;
        public ClaimOrganizationPage _claimOrganizationPage;
        public AddClaimBrandingPage _addClaimBrandingPage;
        public AddBrandingPage _addBrandingPage;
        public AddLocationPage _addLocationPage;
        public AddServicesPage _addServicesPage;
        public AddStaffPage _addStaffPage;

        public AddStaffSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);
            //_gmailPage = new YahooPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _claimOrganizationPage = new ClaimOrganizationPage(WebDriver);
            _addClaimBrandingPage = new AddClaimBrandingPage(WebDriver);
            _addBrandingPage = new AddBrandingPage(WebDriver);
            _addLocationPage = new AddLocationPage(WebDriver);
            _addServicesPage = new AddServicesPage(WebDriver);
            _addStaffPage = new AddStaffPage(WebDriver);
        }

        [Given(@"As an administrator provider of an organization\(NDIS registered or Unregistered\) launch “MyChooser” Website")]
        public void GivenAsAnAdministratorProviderOfAnOrganizationNDISRegisteredOrUnregisteredLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"Provider Register with MyChooser as a User")]
        public void WhenProviderRegisterWithMyChooserAsAUser()
        {
            _userRegistrationPage.CallRegisterButtn();
            _userRegistrationPage.FillRegistrationForm();
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            //_gmailPage.GmailLogin();
            //_gmailPage.YahooLogin();
            WebDriver.Navigate().Refresh();
            _gmailPage.OpenRegistrationEmail();
            //_gmailPage.CompleteSubject();
            _gmailPage.CompleteRegistrationMyChooser();
            _userRegistrationPage.ChoosePassword();
        }

        [When(@"Logs in as provider administrator for the first time into provider organization account")]
        public void WhenLogsInAsProviderAdministratorForTheFirstTimeInToProviderOrganizationAccount()
        {
            //_loginPage.CallSignPage();
            _userRegistrationPage.LinkText();
            _userRegistrationPage.SigninToApplication();
        }

        [When(@"Creates an Organisation")]
        public void WhenCreatesAnOrganisation()
        {
            _providerPage.SelectProvider();
            _providerPage.SearchOrganizationByName();
            _providerPage.RegisterNewOrganizationOption();
            _providerPage.SelectRegisterOrganization();
            _providerPage.EnterRegistrationDetails();
            _userRegistrationPage.EmailORG();
            _providerPage.SubmitRegistrationForm();
        }
        //[When(@"Creates an Organization, Completes claim process, Adds Branding, Adds Location, Adds Services")]
        //public void WhenCreatesAnOrganizationCompletesClaimProcessAddsBrandingAddsLocationAddsServices()
        //{
        //    _providerPage.SelectProvider();
        //    _providerPage.SearchOrganizationByName();
        //    _providerPage.RegisterNewOrganizationOption();
        //    _providerPage.SelectRegisterOrganization();
        //    _providerPage.EnterRegistrationDetails();
        //    _providerPage.SubmitRegistrationForm();
        //    _claimOrganizationPage.EnterClaimDetailsWOSecurityCode();

        //}
        [When(@"Completes claim process with received Security Code, Adds Branding, Adds Location")]
        public async Task WhenCompletesClaimProcessWithReceivedSecurityCodeAddsBrandingAddsLocation()
        {
            _providerPage.SelectProvidersSearch();
            //_providerPage.SearchOrgPage();
            //_providerPage.SelectProviderSearch();
            _providerPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _providerPage.GetSecurityCodeAsync();
            _providerPage.ClaimTheOrganizationWithSecurityCode();
            _addBrandingPage.SelectSetUpMyProfileOption();
            _addClaimBrandingPage.AddClaimOrganizationDetails();
            _addClaimBrandingPage.SubmitClaimOrganizationDetails();
            _addLocationPage.SelectAddLocationButton();
            _addLocationPage.EnterLocContactDetails();
            _addLocationPage.SelectSave();
        }

        [When(@"submits “Continue” request with chosen Services")]
        public void WhenSubmitsContinueRequestWithChosenServices()
        {
            _addServicesPage.SelectAddServices();
            _addServicesPage.AddSupportCategories();
            _addServicesPage.AddRegisteredGroups();
            _addServicesPage.SelectContinue();
        }


        [Then(@"provider should be suggested with “Add Staff” Button, “Do this later” link-text")]
        public void ThenProviderShouldBeSuggestedWithAddStaffButtonDoThisLaterLink_Text()
        {
            //Assert.IsTrue(_addServicesPage.AddStaffbutton());
            //Assert.IsTrue(_addServicesPage.DoitlaterLinkText());
            if (_addServicesPage.AddStaffbutton())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider has Added Branding, Location and Services", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to Add Branding, Location and Services", null);
            }

            if (_addServicesPage.DoitlaterLinkText())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view DoThisLater Link-Text", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to view DoThisLater Link-Text", null);
            }
        }

        [When(@"provider clicks on “Add Staff” button")]
        public void WhenProviderClicksOnAddStaffButton()
        {
            _addStaffPage.SelectAddStaff();
        }

        [Then(@"provider should be redirected to “Add Support Worker” page")]
        public void ThenProviderShouldBeRedirectedToAddSupportWorkerPage()
        {
            Assert.IsTrue(_addStaffPage.AddStaff());
        }

        [When(@"provider submits “Continue” request with required suport worker details, chosen support services")]
        public void WhenProviderSubmitsContinueRequestWithRequiredSuportWorkerDetailsChosenSupportServices()
        {
            _addStaffPage.EnterSupportWorkerDetails();
        }
            
        [Then(@"provider should be redirected to new page suggesting provider with “Add Another Person” button, “Finish” option")]
        public void ThenProviderShouldBeRedirectedToNewPageSuggestingProviderWithAddAnotherPersonButtonFinishoption()
        {
            //Assert.IsTrue(_addStaffPage.AddAnotherStaffButton());
            if (_addStaffPage.AddAnotherStaffButton())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider has added Support Worker successfully and suggested with AddAnotherStaff, Finish Options", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not suggested with “AddAnotherStaff” button", null);
            }
        }
        [When(@"Provider Clicks on “Add Another Person” button")]
        public void WhenProviderClicksOnAddAnotherPersonButton()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"Provider should be redirected to Support Worker Page")]
        public void ThenProviderShouldBeRedirectedToSupportWorkerPage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"provider submits Continue request with required suport worker details, support services")]
        public void WhenProviderSubmitsContinueRequestWithRequiredSuportWorkerDetailsSupportServices()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"provider clicks on “Finish” option")]
        public void WhenProviderClicksOnFinishOption()
        {
            _addStaffPage.SelectFinish();
        }
        
        [When(@"provider submits “Do This Later” link-text")]
        public void WhenProviderSubmitsDoThisLaterLink_Text()
        {
            _addServicesPage.SelectDoitlaterLinkText();
        }

        [Then(@"provider should be redirected to provider Profile Page")]
        public void ThenProviderShouldBeRedirectedToProviderProfilePage()
        {
            if (_providerPage.ConfirmOrganizationName())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view Organization Profile", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view Organization Profile", null);
            }
            //_loginPage.SingOutApp();
        }

        [When(@"Claims an Organisation Without SecurityCode")]
        public void WhenClaimsAnOrganisationWithoutSecurityCode()
        {
            _providerPage.SelectProvider();
            _providerPage.SearchOrgPage();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            _claimOrganizationPage.ClaimTheOrganization();
            Thread.Sleep(4000);
        }

        [When(@"Completes claim process with received Security Code, Adds Branding, Adds Location of claimed organisation")]
        public async Task WhenCompletesClaimProcessWithReceivedSecurityCodeAddsBrandingAddsLocationOfClaimedOrganisation()
        {
            _providerPage.SelectProvider();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _claimOrganizationPage.GetSecurityCodeAsync();
            _claimOrganizationPage.ClaimTheOrganizationWithSecurityCode();
            _addBrandingPage.SelectSetUpMyProfileOption();
            _addClaimBrandingPage.AddClaimOrganizationDetails();
            _userRegistrationPage.EmailORG();
            _addClaimBrandingPage.SubmitClaimOrganizationDetails();
            _addLocationPage.SelectAddLocationButton();
            _addLocationPage.EnterLocContactDetails();
            _addLocationPage.SelectSave();
        }

        [Then(@"provider should be redirected to claimed provider Profile Page")]
        public void ThenProviderShouldBeRedirectedToClaimedProviderProfilePage()
        {
            if (_claimOrganizationPage.ConfirmClaimOrganizationName())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view Claimed Organization Profile", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view Claimed Organization Profile", null);
            }

            _loginPage.SingOutApp();

        }
    }

    
}
