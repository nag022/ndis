﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class ProviderRegistrationSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public GmailPage _gmailPage;
        private UserRegistrationPage _userRegistrationPage;


        public ProviderRegistrationSteps()
        {

            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);

        }

        [Given(@"as a NDIS registered \(Participant or Provider\) or anonymous user launch choices for me website")]
        public void GivenAsANDISRegisteredParticipantOrProviderOrAnonymousProviderLaunchChoicesForMeWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"user cicks on register button")]
        public void WhenUserCicksOnRegisterButton()
        {
            _userRegistrationPage.CallRegisterButtn();
        }

        [Then(@"user should be redirected to registration page")]
        public void ThenUserShouldBeRedirectedToRegistrationPage()
        {
            //_userRegistrationPage.ConfirmRegistrationForm();

            if (_userRegistrationPage.ConfirmRegistrationForm())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can view RegistrationPage", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not view RegistrationPage ", null);
            }
        }

        [When(@"user submits “Register” request with all the required details")]
        public void WhenProviderSubmitsRegisterRequestWithAllTheRequiredDetails()
        {
            _userRegistrationPage.FillRegistrationForm();
        }
        
        
        [Then(@"user should be redirected to new page displaying a message: “confirmation email sent to the registered email”")]
        public void ThenUserShouldBeRedirectedToNewPageDisplayingAMessageConfirmationEmailSentToTheRegisteredEmail()
        {
           //_userRegistrationPage.ConfirmThankYouMessage();

           // if (_userRegistrationPage.ConfirmThankYouMessage())
            if (WebDriver.FindElements(By.XPath("//h1[contains(@class,'col-md-12')]")).ElementAt(0).GetAttribute("innerText").Contains("Thank you for registering with MyChooser"))

            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can view ConfirmRegistrationPage", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not view ConfirmRegistrationPage ", null);
            }
        }

        [When(@"user Login To Gmail, Clicks on Complete Registration button")]
        public void WhenUserLoginToGmailClicksOnCompleteRegistrationButton()
        {
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            //_gmailPage.GmailLogin();
            _gmailPage.OpenRegistrationEmail();
            _gmailPage.CompleteRegistrationMyChooser();
        }

        [Then(@"User Should be redirected to Verify Details Page")]
        public void ThenUserShouldBeRedirectedToVerifyDetailsPage()
        {
            //_userRegistrationPage.VerifyDetails();
        }

        [When(@"Submits Complete Registration request confirming Chosen Password")]
        public void WhenSubmitsCompleteRegistrationRequestConfirmingChosenPassword()
        {
            _userRegistrationPage.ChoosePassword();
        }

        [Then(@"user should be redirected to new page displaying successful registration message with Sign In link-text")]
        public void ThenUserShouldBeRedirectedToNewPageDisplayingSuccessfulRegistrationMessageWithSignInLink_Text()
        {
            if (WebDriver.FindElements(By.XPath("//h1[contains(@class,'col-md-12')]")).ElementAt(0).GetAttribute("innerText").Contains("Congratulations on successfully completing the registration process."))

            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can view ChoosePassword Page", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not view ChoosePassword Page ", null);
            }
        }

        [When(@"User clicks on SignIn link-text")]
        public void WhenUserClicksOnSignInLink_Text()
        {
            _userRegistrationPage.LinkText();
        }

        [Then(@"user should be redirected to Sign In Page")]
        public void ThenUserShouldBeRedirectedToSignInPage()
        {
            _userRegistrationPage.ConfirmRegistration();
        }

        [When(@"User submits Signin request with valid credentials")]
        public void WhenUserSubmitsSigninRequestWithValidCredentials()
        {
            _userRegistrationPage.SigninToApplication();
        }

        [Then(@"User should Signin Successfully")]
        public void ThenUserShouldSigninSuccessfully()
        {
            _userRegistrationPage.ConfirmNDIShomePage();
        }
        

    }
}
