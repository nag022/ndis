﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class AddBrandingSteps 
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public UserRegistrationPage _userRegistrationPage;
        public GmailPage _gmailPage;
        public ProviderSearchPage _providerPage;
        public ClaimOrganizationPage _claimOrganizationPage;
        public AddBrandingPage _addBrandingPage;
        public AddClaimBrandingPage _addClaimBrandingPage;


        public AddBrandingSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _claimOrganizationPage = new ClaimOrganizationPage(WebDriver);
            _addBrandingPage = new AddBrandingPage(WebDriver);
            _addClaimBrandingPage = new AddClaimBrandingPage(WebDriver);
        }

        [Given(@"As a provider administrator \(NDIS registered provider or an unregistered provider\) launch “MyChooser” website")]
        public void GivenAsAProviderAdministratorNDISRegisteredProviderOrAnUnregisteredProviderLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"Provider Register with MyChooser")]
        public void WhenProviderRegisterWithMyChooser()
        {
            _userRegistrationPage.CallRegisterButtn();
            _userRegistrationPage.FillRegistrationForm();
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            //_gmailPage.GmailLogin();
            _gmailPage.OpenRegistrationEmail();
            _gmailPage.CompleteRegistrationMyChooser();
            _userRegistrationPage.ChoosePassword();
        }


        [When(@"provider Logs in as provider administrator for the first time into provider account")]
        public void WhenProviderLogsInAsProviderAdministratorForTheFirstTimeIntoProviderAccount()
        {
           // _loginPage.CallSignPage();
            _userRegistrationPage.SigninToApplication();
        }

       
        [When(@"Creates an Organization")]
        public void WhenCreatesAnOrganization()
        {
            _providerPage.SelectProvider();
            _providerPage.SearchOrganizationByName();
            _providerPage.RegisterNewOrganizationOption();
            _providerPage.SelectRegisterOrganization();
            _providerPage.EnterRegistrationDetails();
            _providerPage.SubmitRegistrationForm();           
        }

        [When(@"completes “Claim this Organization” process with received security code")]
        public async Task WhenCompletesClaimThisOrganizationProcesswithReceivedSecurityCode()
        {
            //_providerPage.SelectProviderSearch();
            _providerPage.SearchOrgPage();
            _providerPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _providerPage.GetSecurityCodeAsync();
            _providerPage.ClaimTheOrganizationWithSecurityCode();
        }

        [Then(@"provider should be suggested with “Setup Organization profile” option")]
        public void ThenProviderShouldBeSuggestedWithSetupOrganizationProfileOption()
        {
            //Assert.IsTrue(_providerPage.SetUpMyProfileOption());
            if (_providerPage.SetUpMyProfileOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can view SetUpMyProfile Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not view SetUpMyProfile Option", null);
            }
        }


        [When(@"provider clicks on “Setup my profile” button")]
        public void WhenProviderClicksOnSetupMyProfileButton()
        {
            _addBrandingPage.SelectSetUpMyProfileOption();
        }

        [Then(@"provider should be redirected to “Add branding” page")]
        public void ThenProviderShouldBeRedirectedToAddBrandingPage()
        {
            Assert.IsTrue(_addBrandingPage.ConfirmClick());
        }
        [When(@"provider enters all the Required Details, Uploads Logo, Banner Image")]
        public void WhenProviderEntersAllTheRequiredDetailsUploadsLogoBannerImage()
        {
            _addBrandingPage.AddOrganizationDetails();
        }

        [When(@"clicks on “submit” button")]
        public void WhenClicksOnSubmitButton()
        {
            _addBrandingPage.SubmitOrganizationDetails();
        }

        [Then(@"provider should be able to Add Organization Branding successfully")]
        public void ThenProviderShouldBeAbleToAddOrganizationBrandingSuccessfully()
        {
            //Assert.IsTrue(_addBrandingPage.ConfirmAddBranding());
            if (_addBrandingPage.ConfirmAddBranding())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User is able to Add Organization Branding successfully", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User is unable to Add Organization Branding successfully", null);
            }
        }

        [Then(@"Should be suggested with AddLocation Button")]
        public void ThenShouldBeSuggestedWithAddLocationButton()
        {
            //Assert.IsTrue(_addBrandingPage.AddLocationOption());
            if (_addBrandingPage.AddLocationOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User suggested with “Add location”", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User is not suggested with “Add location”", null);
            }
        }


        [When(@"Claims Organization Without SecurityCode")]
        public void ClaimsOrganizationWithoutSecurityCode()
        {
            _providerPage.SelectProvider();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            _claimOrganizationPage.ClaimTheOrganization();
        }

        [When(@"completes “Claim Organization” process with received security code")]
        public async Task WhenCompletesClaimOrganizationProcessWithReceivedSecurityCode()
        {
            _providerPage.SelectProviderSearch();
            _providerPage.SearchOrgPage();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _claimOrganizationPage.GetSecurityCodeAsync();
            _claimOrganizationPage.ClaimTheOrganizationWithSecurityCode();
        }

        [When(@"provider enters all the Required Details, Uploads Logo, Uploads Banner Image")]
        public void WhenProviderEntersAllTheRequiredDetailsUploadsLogoUploadsBannerImage()
        {
            _addClaimBrandingPage.AddClaimOrganizationDetails();
        }


    }
}
