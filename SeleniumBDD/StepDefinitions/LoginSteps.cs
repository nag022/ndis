﻿using OpenQA.Selenium;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace SeleniumBDD.PageObjects
{
    [Binding]
    public class Login
    {
        public static IWebDriver WebDriver = TestSetup.WebDriver;
        public LoginPage _loginPage;
        public ProviderSearchPage _providerPage;
        public UserRegistrationPage _userRegistrationPage;

        public Login()
        {
            _loginPage = new LoginPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);

        }

        [Given(@"As a registered user launch choices for me website")]
        public void Asaregistereduserlaunchchoicesformewebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"Logs into provider organization account")]
        public void WhenLogsIntoProviderOrganizationAccount()
        {
            _loginPage.CallSignPage();
            _loginPage.ConfirmSigninPage();
            //_loginPage.LoginToApplication();
            //_providerPage.LoginApplication();
            //_userRegistrationPage.SigninToApplication();
            _loginPage.LoginToApplication();

            _loginPage.ConfirmUserLogin();

        }

        [When(@"User clicks on Signin Option")]
        public void UserclicksonSigninOption()
        {
            _loginPage.CallSignPage();
        }

        [Then(@"User should be redirected to Signin Page")]
        public void UsershouldberedirectedtoSigninPage()
        {
            _loginPage.ConfirmSigninPage();
        }

        [When(@"User submits Signin request with required valid credentils")]
        public void WhenIEnterProviderCredentialsAndSubmit()
        {
            _loginPage.LoginToApplication();
        }

        [Then(@"User should Signin Successfull")]
        public void UsershouldSigninSuccessfull()
        {
            _loginPage.ConfirmUserLogin();
        }


        [When(@"User Clicks on Edit Organization buttto")]
        public void WhenUserClicksOnEditOrganizationButtto()
        {
            _loginPage.clickEditOrganization();
        }

        [Then(@"User should be redirected to Edit Organziation page")]
        public void ThenUserShouldBeRedirectedToEditOrganziationPage()
        {
            _loginPage.Descritionfield();
        }

        [When(@"User submits “Save” request with all the required details")]
        public void WhenUserSubmitsSaveRequestWithAllTheRequiredDetails()
        {
            _loginPage.EDitOrganizationDetails();
        }

        [Then(@"User should be redirected to Profile page")]
        public void ThenUserShouldBeRedirectedToProfilePage()
        {
            _loginPage.EditButton();
        }

        [When(@"USer Clicks on view Button from Location Tile")]
        public void WhenUSerClicksOnViewButtonFromLocationTile()
        {
            _loginPage.selectViewFromMyProfile();
        }

        [Then(@"User should be redirected to all services at this Locations page")]
        public void ThenUserShouldBeRedirectedToAllServicesAtThisLocationsPage()
        {
            Thread.Sleep(10000);
            _loginPage.EditLocationBtn();

        }

        [When(@"User Clicks on Edit Location button")]
        public void WhenUserClicksOnEditLocationButton()
        {
            _loginPage.clickEditLocations();
        }

        [Then(@"User should be able to Edit Location")]
        public void ThenUserShouldBeAbleToEditLocation()
        {
            _loginPage.ConfirmSigninPage();
        }

        [When(@"User subits save request with Edit Location changes")]
        public void WhenUserSubitsSaveRequestWithEditLocationChanges()
        {
            _loginPage.EditLocations_edit();
        }

        [Then(@"User should be redirected back to all services Tab at this Locations page with saved changes")]
        public void ThenUserShouldBeRedirectedBackToAllServicesTabAtThisLocationsPageWithSavedChanges()
        {
            _loginPage.EditLocationBtn();
        }

        [When(@"User Clicks on Services from Location Tab")]
        public void WhenUserClicksOnServicesFromLocationTab()
        {
            _loginPage.NavigateToMyProfileNavLinks("Services");
        }

        [When(@"Submits addServices request")]
        public void WhenSubmitsAddServicesRequest()
        {
            _loginPage.Addservices();
        }

        [Then(@"User should be redirected to add Services page")]
        public void ThenUserShouldBeRedirectedToAddServicesPage()
        {
            _loginPage.SupportPage();
        }

        [When(@"user submits Continue request with chosen support categories, Registered groups")]
        public void WhenUserSubmitsContinueRequestWithChosenSupportCategoriesRegisteredGroups()
        {

            //_loginPage.addServices_edit();
            _loginPage.SelectCategoriesGroups_edit();

        }

        [Then(@"User should be able to Add services successfully")]
        public void ThenUserShouldBeAbleToAddServicesSuccessfully()
        {
            _loginPage.AddServicesBtn();
        }

        [When(@"User Clicks on Edit Service button")]
        public void WhenUserClicksOnEditServiceButton()
        {
            _loginPage.EditService();
        }

        [Then(@"User should be redirected to edit Service page")]
        public void ThenUserShouldBeRedirectedToEditServicePage()
        {
            _loginPage.EditServicePage();
        }

        [When(@"User submits Save request with edited changes")]
        public void WhenUserSubmitsSaveRequestWithEditedChanges()
        {
            _loginPage.EditServices();
        }

        [Then(@"User should be able to save edited services succesfully")]
        public void ThenUserShouldBeAbleToSaveEditedServicesSuccesfully()
        {
            _loginPage.AddServicesBtn();
        }

        [When(@"User Clicks on Staff from Services Tab")]
        public void WhenUserClicksOnStaffFromServicesTab()
        {

            _loginPage.NavigateToMyProfileNavLinks("Staff");
        }

        [When(@"Submit Edit Staff button")]
        public void WhenSubmitEditStaffButton()
        {
            //_loginPage.EditStaffButton();
            _loginPage.SelectAddStaff();
        }

        [Then(@"User should be able to View Edit Staff Page")]
        public void ThenUserShouldBeAbleToViewEditStaffPage()
        {
            _loginPage.EditStaffPage();
        }

        [When(@"User subits save request with Edited Staff changes")]
        public void WhenUserSubitsSaveRequestWithEditedStaffChanges()
        {
            //_loginPage.EditSupportWorkerDetails();
            _loginPage.AddSupportWorkerDetails();

        }

        [Then(@"User should be redirected back to all Staff Tab at this Locations page with saved changes")]
        public void ThenUserShouldBeRedirectedBackToAllStaffTabAtThisLocationsPageWithSavedChanges()
        {
            _loginPage.StaffTabPage();
        }

    }
}



