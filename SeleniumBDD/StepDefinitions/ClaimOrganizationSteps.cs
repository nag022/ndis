﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class ClaimOrganizationSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public UserRegistrationPage _userRegistrationPage;
        public GmailPage _gmailPage;
        public ProviderSearchPage _providerPage;
        public ClaimOrganizationPage _claimOrganizationPage;

        public ClaimOrganizationSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _claimOrganizationPage = new ClaimOrganizationPage(WebDriver);
        }

    
        [Given(@"As an Authorised person \(NDIS registered or unregistered\) of an organization launch “MyChooser” website")]
        public void GivenAsAnAuthorisedPersonNDISRegisteredOrUnregisteredOfAnOrganizationLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"Provider Registers with MyChooser to Claim an Existing Organziation")]
        public void WhenProviderRegistersWithMyChooserToClaimAnExistingOrganziation()
        {
            _userRegistrationPage.CallRegisterButtn();
            _userRegistrationPage.FillRegistrationForm();
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            //_gmailPage.GmailLogin();

            _gmailPage.DeleteRegistrationEmail();
            _gmailPage.OpenRegistrationEmail();
        }

        [When(@"Logs in as provider administrator for the first time")]
        public void WhenLogsInAsProviderAdministratorForTheFirstTime()
        {
            _loginPage.CallSignPage();
            _userRegistrationPage.SigninToApplication();     
                
        }

        [When(@"Authorised person clicks on provider button")]
        public void WhenAuthorisedPersonClicksOnProviderButton()
        {
            _providerPage.SelectProvider();
        }

        [Then(@"Authorised person should be redirected to provider search page")]
        public void ThenAuthorisedPersonShouldBeRedirectedToProviderSearchPage()
        {
            _providerPage.SearchOrgPage();
        }

        [When(@"Authorised person search for an organization by Org name")]
        public void WhenAuthorisedPersonSearchForAnOrganizationByOrgName()
        {
            _claimOrganizationPage.SearchOrganizationByName();
        }

        [Then(@"Authorised person should be able to view the list of Organizations matching the search criteria, Claim option")]
        public void ThenAuthorisedPersonShouldBeAbleToViewTheListOfOrganizationsMatchingTheSearchCriteriaClaimOption()
        {
            //_claimOrganizationPage.ClaimOption();
            if (_claimOrganizationPage.ClaimOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view “Claim” Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to view “Claim” Option", null);
            }
        }

        [When(@"Authorised person clicks on respective organization Claim button")]
        public void WhenAuthorisedPersonClicksOnRespectiveOrganizationClaimButton()
        {
            _claimOrganizationPage.ClaimButton();
        }

        [Then(@"Authorised person should be redirected to claim page")]
        public void ThenAuthorisedPersonShouldBeRedirectedToClaimPage()
        {
            _claimOrganizationPage.ClaimThisOrganization();
        }

        [When(@"Authorised person Claims Organization Without SecurityCode")]
        public void WhenAuthorisedPersonClaimsOrganizationWithoutSecurityCode()
        {
            _claimOrganizationPage.ClaimTheOrganization();
        }

        [When(@"Search agian for the organization to claim with the recieved security code")]
        public void WhenSearchAgianForTheOrganizationToClaimWithTheRecievedSecurityCode()
        {
            _providerPage.SelectProviderSearch();
            _providerPage.SearchOrgPage();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            
        }

        [Then(@"Authorised person should be redirected to claim page with pre-saved details")]
        public void ThenAuthorisedPersonShouldBeRedirectedToClaimPageWithPre_SavedDetails()
        {
            //_claimOrganizationPage.ClaimThisOrganization();
            if (_claimOrganizationPage.ClaimThisOrganization())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view “Claim” Page", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to view “Claim” Page", null);
            }
        }

        [When(@"Authorised person submits “Continue” request with Required Details, Security Code")]
        public async Task WhenAuthorisedPersonSubmitsContinueRequestWithRequiredDetailsSecurityCode()
        {
            String sCode = await _claimOrganizationPage.GetSecurityCodeAsync();
            _claimOrganizationPage.ClaimTheOrganizationWithSecurityCode();
        }

        [Then(@"Authorised person should be suggested with “Setup my profile” option")]
        public void ThenAuthorisedPersonShouldBeSuggestedWithSetupMyProfileOption()
        {
            //_providerPage.SetUpMyProfileOption();
            if (_providerPage.SetUpMyProfileOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider Can view SetUpMyProfile Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider Can not view SetUpMyProfile Option", null);
            }
        }


    }
}
