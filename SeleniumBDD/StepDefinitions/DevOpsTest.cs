﻿using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class DevOpsTest
    {
        public IWebDriver WebDriver;
        public LoginPage _loginPage;
        public ProviderSearchPage _providerPage;
        public DevOpsTestPage _devopsTestPage;
        public ClaimOrganizationPage _claimOrganizationPage;

        
        public DevOpsTest()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _devopsTestPage = new DevOpsTestPage(WebDriver);
            _claimOrganizationPage = new ClaimOrganizationPage(WebDriver);
        }

        [Given(@"As an Anonymous User launch “My Chooser” website")]
        public void GivenAsAnAnonymousUserLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"User clicks on Provider Portal")]
        public void WhenUserClicksOnProviderPortal()
        {
            _providerPage.SelectProvider();
        }

        [Then(@"User should be redirected to Provider Search Page")]
        public void ThenUserShouldBeRedirectedToProviderSearchPage()
        {
            _providerPage.SearchOrgPage();
        }

        [When(@"User Search for a non-existing Organization in My Chooser")]
        public void WhenUserSearchForANon_ExistingOrganizationInMyChooser()
        {
            _providerPage.SearchOrganizationByName();
        }

        [Then(@"User should get an error message:“(.*) is not found\. Click on the register button to add your organisation\.”")]
        public void ThenUserShouldGetAnErrorMessageIsNotFound_ClickOnTheRegisterButtonToAddYourOrganisation_(string p0)
        {
            _providerPage.OrganizationNotAvailableInDataBase();
        }

        [Then(@"Should be suggested with “Register your organization” button")]
        public void ThenShouldBeSuggestedWithRegisterYourOrganizationButton()
        {
            //_providerPage.RegisterNewOrganizationOption();
            if (_providerPage.RegisterNewOrganizationOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User can see the Register NewOrganization Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User can not see the Register NewOrganization Option ", null);
            }
        }

        [Given(@"As an Anonymous User or an authorised person of an organization launch “My Chooser” website")]
        public void GivenAsAnAnonymousUserOrAnAuthorisedPersonOfAnOrganizationLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"User search for an Existing Organization in My Chooser")]
        public void WhenUserSearchForAnExistingOrganizationInMyChooser()
        {
            _providerPage.SelectProvider();
            _devopsTestPage.SearchOrganization();
        }

        [Then(@"User should be able to view list of organizations results matching search criteria, Claim Option")]
        public void ThenUserShouldBeAbleToViewListOfOrganizationsResultsMatchingSearchCriteriaClaimOption()
        {
            //_claimOrganizationPage.ClaimOption();
            if (_claimOrganizationPage.ClaimOption())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is able to view List of Organization matching Search criteria and “Claim” Option", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to view List of Organization matching Search criteria and “Claim” Option", null);
            }
        }

        [When(@"User search for provider services with respective Suburb/Postcode")]
        public void WhenUserSearchForProviderServicesWithRespectiveSuburbPostcode()
        {
            _devopsTestPage.SearchSuburb();
        }

        [Then(@"User should be redirected to Support category services page")]
        public void ThenUserShouldBeRedirectedToSupportCategoryServicesPage()
        {
            _devopsTestPage.SupportCategoryPage();
        }

        [When(@"User submits Continue request with chosen support categories")]
        public void WhenUserSubmitsContinueRequestWithChosenSupportCategories()
        {
            _devopsTestPage.SelectSupportCategoy();
        }

        [Then(@"User should be redirected to registered groups")]
        public void ThenUserShouldBeRedirectedToRegisteredGroups()
        {
            _devopsTestPage.ViewProviderOption();
        }

        [Then(@"Should be able to View Providers button for respective registered group")]
        public void ThenShouldBeAbleToViewProvidersButtonForRespectiveRegisteredGroup()
        {
            _devopsTestPage.ViewProviderOption();
        }

        [When(@"User clicks on View Providers button")]
        public void WhenUserClicksOnViewProvidersButton()
        {
            _devopsTestPage.SelectViewProviders();
            Thread.Sleep(5000);
        }

        [Then(@"User should be able to view list of Provider results providing the chosen service in selected suburb")]
        public void ThenUserShouldBeAbleToViewListOfProviderResultsProvidingTheChosenServiceInSelectedSuburb()
        {
            //_devopsTestPage.Checkbox();
            if (_devopsTestPage.Checkbox())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User is able to view Providers List", null);
            }

            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "User is not able to view Providers List", null);
            }
        }

        [When(@"User clicks on respective View Provider button on provider tile")]
        public void WhenUserClicksOnRespectiveViewProviderButtonOnProviderTile()
        {
            _devopsTestPage.SelectProvider();
        }

        [Then(@"User should be able to view Provider details")]
        public void ThenUserShouldBeAbleToViewProviderDetails()
        {
            //_devopsTestPage.ConfirmProviderPage();
            if (_devopsTestPage.ConfirmProviderPage())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "User is able to view Provider Details", null);
            }

            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not able to view Provider Details", null);
            }
        }

    }
}
