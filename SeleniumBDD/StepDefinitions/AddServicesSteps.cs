﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumBDD.PageObjects;
using SeleniumBDD.Utilities;
using System;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SeleniumBDD.StepDefinitions
{
    [Binding]
    public class AddServicesSteps
    {
        public static IWebDriver WebDriver;
        public LoginPage _loginPage;
        public UserRegistrationPage _userRegistrationPage;
        public GmailPage _gmailPage;
        public ProviderSearchPage _providerPage;
        public ClaimOrganizationPage _claimOrganizationPage;
        public AddClaimBrandingPage _addClaimBrandingPage;
        public AddBrandingPage _addBrandingPage;
        public AddLocationPage _addLocationPage;
        public AddServicesPage _addServicesPage;

        public AddServicesSteps()
        {
            WebDriver = TestSetup.WebDriver;
            _loginPage = new LoginPage(WebDriver);
            _userRegistrationPage = new UserRegistrationPage(WebDriver);
            _gmailPage = new GmailPage(WebDriver);
            _providerPage = new ProviderSearchPage(WebDriver);
            _claimOrganizationPage = new ClaimOrganizationPage(WebDriver);
            _addClaimBrandingPage = new AddClaimBrandingPage(WebDriver);
            _addBrandingPage = new AddBrandingPage(WebDriver);
            _addLocationPage = new AddLocationPage(WebDriver);
            _addServicesPage = new AddServicesPage(WebDriver);

        }


        [Given(@"As an Administrator Provider \(NDIS registered or Unregistered provider\) of an organization launch “MyChooser” website")]
        public void GivenAsAnAdministratorProviderNDISRegisteredOrUnregisteredProviderOfAnOrganizationLaunchMyChooserWebsite()
        {
            _loginPage.LaunchApplication();
        }

        [When(@"Authorised person Registers with MyChooser")]
        public void WhenAuthorisedPersonRegistersWithMyChooser()
        {
            _userRegistrationPage.CallRegisterButtn();
            _userRegistrationPage.FillRegistrationForm();
            _gmailPage.launchGmail();
            _gmailPage.GmailLogin(1);
            //_gmailPage.GmailLogin();

            _gmailPage.OpenRegistrationEmail();
            _gmailPage.CompleteRegistrationMyChooser();
            _userRegistrationPage.ChoosePassword();
        }

        [When(@"Logs in for the first time into provider accoount")]
        public void WhenLogsInForTheFirstTimeIntoProviderAccoount()
        {
            //_loginPage.CallSignPage();
            _userRegistrationPage.SigninToApplication();

        }

        [When(@"Creates  Organization")]
        public void WhenCreatesOrganization()
        {
            _providerPage.SelectProvider();
            _providerPage.SearchOrganizationByName();
            _providerPage.RegisterNewOrganizationOption();
            _providerPage.SelectRegisterOrganization();
            _providerPage.EnterRegistrationDetails();
            _providerPage.SubmitRegistrationForm();
        }

        [When(@"Completes claim process with received Security Code, Adds Branding of the Organization")]
        public async Task WhenCompletesClaimProcessWithReceivedSecurityCodeAddsBrandingOfTheOrganization()
        {
            //_providerPage.SelectProviderSearch();
            _providerPage.SearchOrgPage();
            _providerPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _providerPage.GetSecurityCodeAsync();
            _providerPage.ClaimTheOrganizationWithSecurityCode();
            _addBrandingPage.SelectSetUpMyProfileOption();
            _addBrandingPage.AddOrganizationDetails();
            _addBrandingPage.SubmitOrganizationDetails();
        }

        [When(@"submits “Save” request with required details on Add Location page")]
        public void WhenSubmitsSaveRequestWithRequiredDetailsOnAddLocationPage()
        {
            _addLocationPage.SelectAddLocationButton();
            _addLocationPage.EnterLocContactDetails();
            _addLocationPage.SelectSave();
        }


        [Then(@"provider should be suggested with “Add another Location”, “Add Services” button")]
        public void ThenProviderShouldBeSuggestedWithAddAnotherLocationAddServicesButton()
        {
            Assert.IsTrue(_addLocationPage.AddServicesButton());
            Assert.IsTrue(_addLocationPage.AddAnotherLocationButton());
        }

        [When(@"provider clicks on “Add services” button")]
        public void WhenProviderClicksOnAddServicesButton()
        {
            _addServicesPage.SelectAddServices();
        }

        [Then(@"provider should be redirected to “Support Category” page")]
        public void ThenProviderShouldBeRedirectedToSupportCategoryPage()
        {
            Assert.IsTrue(_addServicesPage.SupportCategories());
        }

        [When(@"Provider submits “Continue” request with chosen support categories")]
        public void WhenProviderSubmitsContinueRequestWithChosenSupportCategories()
        {
            _addServicesPage.AddSupportCategories();
            
        }

        [Then(@"provider should be redirected to “Registed Groups” page")]
        public void ThenProviderShouldBeRedirectedToRegistedGroupsPage()
        {
            Assert.IsTrue(_addServicesPage.RegisteredGroups());
        }

        [When(@"Provider submits “Continue” request with chosen registered groups")]
        public void WhenProviderSubmitsContinueRequestWithChosenRegisteredGroups()
        {
            _addServicesPage.AddRegisteredGroups();
            _addServicesPage.SelectContinue();
        }
         
        [Then(@"provider should be suggested with “Add Staff” button, “Do this later” link-text")]
        public void ThenProviderShouldBeSuggestedWithAddStaffButtonDoThisLaterLink_Text()
        {
            //Assert.IsTrue(_addServicesPage.AddStaffbutton());
            if (_addServicesPage.AddStaffbutton())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is suggested with “AddStaff” button", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not suggested with “AddStaff” button", null);
            }

            //Assert.IsTrue(_addServicesPage.DoitlaterLinkText());
            if (_addServicesPage.DoitlaterLinkText())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider suggested with “Doitlater” LinkText", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not suggested with “Doitlater” LinkText", null);
            }
            
        }

        [When(@"provider clicks on “Do This Later” link-text")]
        public void WhenProviderClicksOnDoThisLaterLink_Text()
        {
            _addServicesPage.SelectDoitlaterLinkText();
        }

        [Then(@"provider should be redirected to provider\(Organization\) Profile Page")]
        public void ThenProviderShouldBeRedirectedToProviderOrganizationProfilePage()
        {
            //Assert.IsTrue(_providerPage.ConfirmOrganizationName());
            if (_providerPage.ConfirmOrganizationName())
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Pass, "Provider is redirected to “Profile Page”", null);
            }
            else
            {
                TestSetup.currentTest.Log(AventStack.ExtentReports.Status.Fail, "Provider is not redirected to “Profile Page”", null);
            }
        }

        [When(@"Claims an Organization Without SecurityCode")]
        public void WhenClaimsAnOrganizationWithoutSecurityCode()
        {
            _providerPage.SelectProvider();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            _claimOrganizationPage.ClaimTheOrganization();
        }


        [When(@"Completes claim process with received Security Code, Adds Branding of the claimed Organization")]
        public async Task WhenCompletesClaimProcessWithReceivedSecurityCodeAddsBrandingOfTheClaimedOrganization()
        {
            _providerPage.SelectProviderSearch();
            _providerPage.SearchOrgPage();
            _claimOrganizationPage.SearchOrganizationByName();
            _claimOrganizationPage.ClaimButton();
            String sCode = await _claimOrganizationPage.GetSecurityCodeAsync();
            _claimOrganizationPage.ClaimTheOrganizationWithSecurityCode();
            _addBrandingPage.SelectSetUpMyProfileOption();
            _addClaimBrandingPage.AddClaimOrganizationDetails();
            _addClaimBrandingPage.SubmitClaimOrganizationDetails();
        }


    }
}
