﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumBDD.Utilities
{
    public class TableElement
    {
        private readonly IWebElement _webTable;
        private readonly IList<IWebElement> _rowElements;
        private readonly By _byRowTag = By.XPath("./tr|./tbody/tr|./tfoot/tr|./thead/tr");
        private readonly By _byColumnTag = By.XPath("./td|./th");

        public int RowCount
        {
            get { return _rowElements.Count; }
        }

        public int ColumnCount(int rowValue = 0)
        {
            if (RowCount > rowValue)
                return _rowElements[rowValue].FindElements(_byColumnTag).Count;
            return -1;
        }

        public TableElement(IWebElement webTable)
        {
            if (String.Compare(webTable.TagName, "Table", StringComparison.OrdinalIgnoreCase) != 0)
                throw new Exception("Argument injected is not a table object");
            _webTable = webTable;
            _rowElements = _webTable.FindElements(_byRowTag);
        }

        public string GetCellData(int row, int col)
        {
            if (RowCount <= row)
                throw new Exception("Total table rows are less than the passed row argument-" + row);
            if (ColumnCount(row) <= col)
                throw new Exception("Total table columns are less than the passed column argument-" + col);
            return _rowElements[row].FindElements(_byColumnTag)[col].Text;
        }


        public int GetRowWithCellText(string text, StringComparison comparision = StringComparison.OrdinalIgnoreCase, int startRow = 0)
        {
            if (RowCount <= startRow)
                return -1;
            foreach (IWebElement rowElement in _rowElements)
                foreach (IWebElement colElement in rowElement.FindElements(_byColumnTag))
                {
                    // if (String.Compare(text.Trim(), colElement.Text.Trim(), comparision) == 0)
                    if (colElement.Text.Trim().Contains(text))
                    {
                        if (_rowElements.IndexOf(rowElement) >= startRow) return _rowElements.IndexOf(rowElement);
                    }
                }
            return -1;
        }

        public IWebElement ChildItem(int row, int col, By by, int index = 0)
        {
            if (RowCount <= row)
                throw new NoSuchElementException("Total table rows are less than the passed row argument-" + row);
            var columnElements = _rowElements[row].FindElements(_byColumnTag);
            if (columnElements.Count > col)
            {
                var tempElements = columnElements[col].FindElements(@by);
                if (tempElements.Count == 0)
                    throw new NoSuchElementException("");
                if (tempElements.Count > index)
                    return tempElements[index];
                throw new IndexOutOfRangeException("");
            }
            throw new NoSuchElementException("Total table columns are less than the passed column argument-" + col);
        }

        public int ChildItemsCount(int row, int col, By by)
        {
            if (RowCount <= row)
                throw new NoSuchElementException("Total table rows are less than the passed row argument-" + row);
            var columnElements = _rowElements[row].FindElements(_byColumnTag);
            if (columnElements.Count > col)
            {
                var tempElements = columnElements[col].FindElements(@by);
                return tempElements.Count;
            }
            throw new NoSuchElementException("Total table columns are less than the passed column argument-" + col);
        }

        public int GetColumnWithCellText(string text, StringComparison comparision = StringComparison.OrdinalIgnoreCase, int startRow = 0)
        {
            var rowNumber = GetRowWithCellText(text.Trim(), comparision, startRow);
            if (rowNumber != -1)
            {
                var colElements = _rowElements[rowNumber].FindElements(_byColumnTag);
                foreach (var colElement in colElements.Where(colElement => String.Compare(colElement.Text.Trim(), text.Trim(), comparision) == 0))
                    return colElements.IndexOf(colElement);
            }
            return -1;
        }

    }
}
