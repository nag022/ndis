﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumBDD.Utilities
{
    public class GeneralUtility
    {
        static Random rnd = new Random();
        public static int randumNumber()
        {
            return rnd.Next(0000, 9999);
        }

        public static string RandomAlphaNumeric()
        {
            
            {
                var chars = "abcdefghijklmnopqrstuvwxyz";
                return new string(chars.Select(c => chars[rnd.Next(chars.Length)]).Take(8).ToArray());
            }
        }
    }



}
